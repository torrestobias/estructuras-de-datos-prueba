data RobotAprendiz = RA (Map Habilidad (Heap Instruccion)) (MultiSet Habilidad) (Maybe Habilidad)

--A)
emptyRA :: RobotAprendiz
emptyRA = RA (emptyM) (emptyMS) (Nothing)

--B)
habilidadesEn :: RobotAprendiz -> [Habilidad]
habilidadesEn (RA map multi maybe) = (domM map)

--C)
registrarHabildad :: Habilidad -> [Instruccion] -> RobotAprendiz -> RobotAprendiz --O(n.logN)
registrarHabildad h ins (RA map multi maybe) = (RA (agregarEnMap h ins map) (addMS h multi) (Just h))

agregarEnMap :: Habilidad -> [Instruccion] -> Map Habilidad (Heap Instruccion) -> Map Habilidad (Heap Instruccion) --O(n.log N)
agregarEnMap h [] map = map
agregarEnMap h (i:is) map = assocM h (insertH i (fromJust(lookupM map h))) (agregarEnMap h is map)

--D)
conoceHabilidad :: Habilidad -> RobotAprendiz -> Bool --O(logN)
conoceHabilidad h (RA map multi maybe) = conoceLaHabilidad h multi

conoceLaHabilidad :: Habilidad -> MultiSet Habilidad -> Bool
conoceLaHabilidad h multi = if 

--E)
resultadoDe :: Habilidad -> RobotAprendiz -> Maybe Resultado -- HAblar con el profe sobre la eficiencia
resultadoDe h (RA map multi maybe) = Just (mkResultado(heapALista(fromJust(lookupM map h))))

heapALista :: Ord a => Heap a -> [a] --O(logN)
heapALista h = if isEmpty h
				then []
				else findMin h : (heapALista (deleteMin h))

--F)
mejorarHabilidades :: [Habilidad] -> [Instruccion] -> RobotAprendiz -> RobotAprendiz
mejorarHabilidades (h:hbs) ins (RA map multi maybe) = RA (registrarHabildad h ins)

--G)
masRecurrente :: RobotAprendiz -> Maybe Habilidad
masRecurrente (RA map multi maybe) = maybe

--F)
mejorarHabilidades :: [Habilidad] -> [Instruccion] -> RobotAprendiz -> RobotAprendiz
mejorarHabilidades [] xs robot = robot
mejorarHabilidades (h:hs) xs robot = mejorarHabilidades hs xs (registrarHabildad h xs robot)
