type Software = String
data Autor = Admin String | Dev String
data Organizador = Agregar Software [Autor] Organizador | Vacio

--1.1
--dado un organizador, denota el conjunto de pares programa y cantidad de autores que existen.
pares :: Organizador -> [(Software, Int)]
pares Vacio = []
pares (Agregar programa (x:xs) organizador) = (programa,x) : (pares xs organizador)

--1.2
