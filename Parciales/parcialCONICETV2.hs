type Titulo = String
type Autor = String
type Fecha = Int
data Paper = P Titulo Fecha deriving (Show,Eq)
data Grupo = Vacio
			| Becario Autor [Paper]
			| Investigador Autor [Paper] Grupo Grupo Grupo

data Conicet = C [Grupo]


--a)
becarios :: Conicet -> [Autor]
becarios (C gs) = becariosGs gs

becariosGs :: [Grupo] -> [Autor]
becariosGs [] = []
becariosGs (g:gs) = (nombreBecariosGs g) ++ (becariosGs gs)

nombreBecariosGs :: Grupo -> [Autor]
nombreBecariosGs Vacio = []
nombreBecariosGs (Becario a papers) = [a]
nombreBecariosGs (Investigador a papers g1 g2 g3) = (nombreBecariosGs g1) ++ (nombreBecariosGs g2) ++ (nombreBecariosGs g3)

--b)
autoresDe :: Paper -> Conicet -> [Autor]
autoresDe p (C gs) = autoresDeGs p gs

autoresDeGs :: Paper -> [Grupo] -> [Autor]
autoresDeGs p [] = []
autoresDeGs p (g:gs) = (autoresDelPaper p g) ++ (autoresDeGs p gs)

autoresDelPaper :: Paper -> Grupo -> [Autor]
autoresDelPaper p Vacio = []
autoresDelPaper p (Becario a papers) = if (elem p papers)
										then [a]
										else []
autoresDelPaper p (Investigador a papers g1 g2 g3) = if (elem p papers)
											 then [a] ++ ((autoresDelPaper p g1) ++ (autoresDelPaper p g2) ++ (autoresDelPaper p g3))
											 else (autoresDelPaper p g1) ++ (autoresDelPaper p g2) ++ (autoresDelPaper p g3)

--c)
soloSigloXXI :: Conicet -> Conicet
soloSigloXXI (C gs) = C (soloSigloXXIGs gs)

soloSigloXXIGs :: [Grupo] -> [Grupo]
soloSigloXXIGs [] = []
soloSigloXXIGs (g:gs) = (sinPapers g) : (soloSigloXXIGs gs) 

sinPapers :: Grupo -> Grupo
sinPapers Vacio = Vacio
sinPapers (Becario a papers) = (Becario a (sacarPapersSigloXXI papers))
sinPapers (Investigador a papers g1 g2 g3) = (Investigador a (sacarPapersSigloXXI papers) (sinPapers g1) (sinPapers g2) (sinPapers g3))

fechaDelPaper :: Paper -> Int
fechaDelPaper (P titulo anio) = anio

sacarPapersSigloXXI :: [Paper] -> [Paper]
sacarPapersSigloXXI [] = []
sacarPapersSigloXXI (x:xs) = if ((fechaDelPaper x) > 2000)
								then x : (sacarPapersSigloXXI xs)
								else (sacarPapersSigloXXI xs)

--d)


jefesDelBecario :: Autor -> Grupo -> [Autor]
--devuelve el nombre de los autores que poseen a ese becario como subordinado
jefesDelBecario n Vacio = []
jefesDelBecario n (Becario a papers) = []
jefesDelBecario n (Investigador a papers g1 g2 g3) = if (hayBecario n g1)
														then [n] ++ (jefesDelBecario a g1) ++ (jefesDelBecario a g2) ++ (jefesDelBecario a g3)
														else (jefesDelBecario a g1) ++ (jefesDelBecario a g2) ++ (jefesDelBecario a g3)

hayBecario :: Autor -> Grupo -> Bool
hayBecario n Vacio = False
hayBecario n (Becario a papers) = (tieneMismoNombre n a)
hayBecario n (Investigador a papers g1 g2 g3) = (hayBecario n g1) || (hayBecario n g2) || (hayBecario n g3)

tieneMismoNombre :: Autor -> Autor -> Bool
tieneMismoNombre a1 a2 = a1 == a2

--e)
provomerBecarios :: Grupo -> [Autor] -> Grupo
provomerBecarios Vacio autores = Vacio
provomerBecarios (Becario a papers) autores = if elem a autores
												then (Investigador a papers Vacio Vacio Vacio)
												else (Becario a papers)
provomerBecarios (Investigador a papers g1 g2 g3) autores = (Investigador a papers (provomerBecarios g1 autores) (provomerBecarios g2 autores) (provomerBecarios g3 autores))												 

