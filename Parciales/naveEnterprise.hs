data Nave = MkN (Map Sector (Set Tripulante)) (Heap Tripulante) (Sector, Int)

-- rango :: Tripulante -> Rango

conRango :: Rango -> Nave -> Set Tripulante
conRango r (MkN map heap par) = agregarTripulantes' (todosLosTripulantes heap) r

agregarTripulantes' :: [Tripulante] -> Rango -> Set Tripulante --O (N.logN)
agregarTripulantes' [] r = emptyS
agregarTripulantes' (t:ts) r = if (rango t == r)
								then addS t (agregarTripulantes' ts r)
								else (agregarTripulantes' ts r)

todosLosTripulantes :: Heap Tripulante -> [Tripulante] --O(logN)
todosLosTripulantes heap = (findMin heap) : (todosLosTripulantes deleteMin heap)

--------------------------------------------------------------------------

sectorDe :: Tripulante -> Nave -> Sector
sectorDe t nave = sectores' (sectores nave) t nave

sectores' :: [Sectores] -> Tripulante -> Nave -> Sector
sectores' (s:ss) t nave = if (belongs t (tripulantesDe s nave))
							then sectorDe t nave
							else (sectores' ss t nave) 

--------------------------------------------------------------------------

agregarTripulantes :: Tripulante -> Sector -> Nave -> Nave
agregarTripulantes t s (MkN map heap par) = MkN (agregarAlMap t s map) (insertH t heap) (aumentarSector par)  

agregarAlMap :: Tripulante -> Sector -> Map Sector (Set Tripulante) -> Map Sector (Set Tripulante)
agregarAlMap t s map = assocM s (addS t (fromJust (lookupM map s))) map

aumentarSector :: Sector -> (Sector,Int) -> (Sector,Int)
aumentarSector s par = if (s == (fst par))
						then (fst par, (snd par) +1)
						else par