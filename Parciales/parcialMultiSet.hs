﻿data MultiSet = MkMS (Map Int (Set a)) (Set a)

-- Las claves del map representan la cantidad de apariciones
-- que tienen los elementos del set

-- El set de MkMS representa el conjunto de elementos
-- que más veces aparece en el multiset

-- Inv. Rep.
-- En el map no se guardan sets vacíos
-- Un elemento no puede aparecer en más de un set
-- En el set de MkMS aparecen los elementos con mayor clave del map

-- Propósito: Crea un MultiSet vacío
emptyMS :: MultiSet a -- O(1)
emptyMS = MkMS emptyM emptyS

-- Propósito: Retorna el conjunto de elementos que aparece
-- una cierta cantidad de veces
appearsNTimes :: Ord a => Int -> MultiSet a -> [a] --O(N.logN)
appearsNTimes n (MkMS map set) = setToList(fromJust(lookupM map n)) 

-- Propósito: Retorna la cantidad de veces que aparece un elemento
ocurrencesMS :: Ord a => a -> MultiSet a -> Int --O()
ocurrencesMS a (MkMS map set) = (cantAparicionesDelElemento a (domM map) map)

cantAparicionesDelElemento :: Eq a => a -> [Int] -> Map Int (Set a) -> Int --O(N.logN^2 ?)
cantAparicionesDelElemento a map [] = 0
cantAparicionesDelElemento a map (k:ks) = if (belongs a fromJust(lookupM map k))
											then k
											else cantAparicionesDelElemento a map ks

-- Propósito: Agrega un elemento a un multiset
addMS :: Ord a => a -> MultiSet a -> MultiSet a --O(N.logN^2 ?)
addMS a (MkMS map set) = (MkMS (agregarAl map a (domM map)) set)

agregarAlMap :: Map Int (Set a) -> a -> [Int] -> Map Int (Set a) --O(N.logN^2 ?)
agregarAlMap map a [] = map
agregarAlMap map a (k:ks) = if (belongs a fromJust(lookupM map k))
								then assocM (k+1) a map
								else agregarAlMap map a ks  

-- Propósito: Retorna una lista de pares
-- donde la primera componente es la cantidad de veces
-- que aparecen los elementos de la segunda componente
appearances :: MultiSet a -> [(Int,[a])]

-- Propósito: Retorna el conjunto de elementos que
-- más veces aparece
maxMS :: MultiSet a -> Set a -- O(1)