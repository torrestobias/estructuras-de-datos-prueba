data Arbol a = Vacio | Nodo a (Arbol a) (Arbol a) deriving (Show,Eq)
data Dir = Izq | Der deriving (Show,Eq)
data Celda = ConsCelda Int Int deriving (Show,Eq)

arbol1 :: Arbol Celda 
arbol1 = Nodo (ConsCelda 10 10) ( Nodo (ConsCelda 1 2) Vacio (Nodo (ConsCelda 2 5) Vacio (Vacio)) ) (Nodo (ConsCelda 2 3) Vacio Vacio)

--1.1

existeCamino :: [Dir] -> Arbol Celda -> Bool
--Dado un camino y un arbol, indica si ese camino existe en el arbol. Un camino existe si puedo completarlo sin que se termine el arbol.
existeCamino dirs Vacio = False
existeCamino [] (Nodo x t1 t2) = True
existeCamino (dir:dirs) (Nodo x t1 t2) = hayCamino dir (Nodo x t1 t2) && (existeCamino dirs t1 || existeCamino dirs t2)


hayCamino :: Dir -> Arbol Celda -> Bool
hayCamino d Vacio = False
hayCamino d (Nodo x t1 t2) = if (d == Izq)
								then t1 /= Vacio 
								else t2 /= Vacio

--1.2

rojasDeCelda :: [Dir] -> Arbol Celda -> Int
rojasDeCelda dirs Vacio = 0
rojasDeCelda [] (Nodo x t1 t2) = cantidadRojas x
rojasDeCelda (d:dirs) (Nodo x t1 t2) = rojasDeCelda dirs ( avance d (Nodo x t1 t2) )

avance :: Dir -> Arbol Celda -> Arbol Celda
avance d Vacio = Vacio
avance d (Nodo x t1 t2) = if (Izq == d) then t1 else t2


cantidadRojas :: Celda -> Int
cantidadRojas (ConsCelda v r) = r

--1.3

celdaConMasRojas :: Arbol Celda -> Celda
celdaConMasRojas Vacio = error "no se puede, no va a ser Ez"
celdaConMasRojas (Nodo x t1 t2) = arbolCelda x (celdaConMasRojas t1) (celdaConMasRojas t2) 

arbolCelda :: Arbol Celda -> Celda
arbolCelda (Nodo x t1 t2) = x

mayorCantidadDeRojas :: Celda -> Celda -> Celda
mayorCantidadDeRojas c1 c2 = if ( cantidadRojas c1 > cantidadRojas c2)
								then c1
								else c2
