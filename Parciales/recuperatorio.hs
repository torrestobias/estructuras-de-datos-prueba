descomprimir :: [(Int,a)] -> [a]
descomprimir [] = []
descomprimir (x:xs) = (agregarNVeces (fst x) (snd x)) ++ (descomprimir xs)

agregarNVeces :: Int -> a -> [a]
agregarNVeces 0 a = []
agregarNVeces n a = a : agregarNVeces (n - 1) a