data Contenedor = Comida | Oxigeno | Torpedo | Combustible deriving (Show, Eq)

data Componente = Escudo | CanionLaser | Lanzatorpedos | Motor Int | Almacen [Contenedor] deriving (Show,Eq)

data Nave = Parte Componente Nave Nave | ParteBase deriving (Show,Eq)

data Tree a = EmptyT | NodeT a (Tree a)(Tree a) deriving (Show,Eq)

nave :: Nave
nave = Parte (Motor 5)
					(Parte (Almacen [Comida]) ParteBase ParteBase)
					(Parte (Almacen [Comida,Comida]) (Parte CanionLaser ParteBase ParteBase) 
						ParteBase)

--a)
componentes :: Nave -> [Componente]
componentes ParteBase = []
componentes (Parte c n1 n2) = c : ((componentes n1) ++ (componentes n2))

--b)
poderDePropulsion :: Nave -> Int
poderDePropulsion ParteBase = 0
poderDePropulsion (Parte c n1 n2) = (poderDelMotor c) + (poderDePropulsion n1) + (poderDePropulsion n2)
									 

poderDelMotor :: Componente -> Int
poderDelMotor (Motor n) = n
poderDelMotor _ = 0


--c)
desarmarse :: Nave -> Nave
desarmarse ParteBase = ParteBase
desarmarse (Parte c n1 n2) = if (esArma c)
								then (Parte Escudo (desarmarse n1) (desarmarse n2))
								else (Parte c (desarmarse n1) (desarmarse n2))

esArma :: Componente -> Bool
esArma CanionLaser = True
esArma Lanzatorpedos = True
esArma _ = False

--d)
cantidadDeComida :: Nave -> Int
cantidadDeComida ParteBase = 0 
cantidadDeComida (Parte c n1 n2) = if (esAlmacen c)
								 	then (cantidadComidita (listaDeContenedor c)) + (cantidadDeComida n1) + (cantidadDeComida n2)
								 	else cantidadDeComida n1 + cantidadDeComida n2

esAlmacen :: Componente -> Bool
esAlmacen (Almacen ls) = True
esAlmacen _ = False

listaDeContenedor :: Componente -> [Contenedor]
listaDeContenedor (Almacen ls) = ls
listaDeContenedor _ = []

esComida :: Contenedor -> Bool
esComida Comida = True
esComida _ = False

cantidadComidita :: [Contenedor] -> Int
cantidadComidita [] = 0
cantidadComidita (x:xs) = if ( esComida x)
							then 1 + (cantidadComidita xs)
							else cantidadComidita xs

--e)

naveToTree :: Nave -> Tree Componente
naveToTree ParteBase = EmptyT
naveToTree (Parte c n1 n2) = (NodeT c (naveToTree n1) (naveToTree n2))

--f)

aprovisionados :: [Contenedor] -> Nave -> Bool
aprovisionados [] ParteBase = False
aprovisionados ls (Parte c n1 n2) = (compararContenedor ls (listaDeContenedor c)) && ((aprovisionados ls n1) && (aprovisionados ls n2))

	

compararContenedor :: [Contenedor] -> [Contenedor] ->Bool
compararContenedor [] [] = True
compararContenedor [] cs = True
compararContenedor ls [] = False
compararContenedor (l:ls) cs = (elem l cs) && (compararContenedor ls cs)

--g)

armasNivelN :: Int -> Nave -> [Componente]
armasNivelN n ParteBase = []
armasNivelN n (Parte c n1 n2) = if ((esArma c) && (n == 0))
								 then [c]
								 else (armasNivelN (n-1) n1) ++ (armasNivelN (n-1) n2)