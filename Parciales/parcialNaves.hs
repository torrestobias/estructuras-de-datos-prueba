data Contenedor = Comida | Oxigeno | Torpedo | Combustible deriving (Show,Eq)
data Componente = Escudo | CanionLaser | Lanzatorpedos | Motor Int | Almacen [Contenedor] deriving (Show,Eq)
data Nave = Parte Componente Nave Nave | ParteBase deriving (Show,Eq)

data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving (Show,Eq)


--1.1

nave1 :: Nave 
nave1 = Parte (Motor 6) (Parte Escudo (Parte (Almacen [Comida,Combustible,Oxigeno]) ParteBase ParteBase)(Parte CanionLaser ParteBase ParteBase)) ( Parte (Almacen[Comida, Combustible,Oxigeno, Torpedo,Comida]) ParteBase ParteBase)

--1.2

componentes :: Nave -> [Componente]
componentes ParteBase = []
componentes (Parte comp t1 t2) = (comp) : (componentes t1) ++ (componentes t2)  

--1.3

poderDePropulsion :: Nave -> Int
poderDePropulsion ParteBase = 0
poderDePropulsion (Parte (Motor n) t1 t2) = (n) +  (poderDePropulsion t1) + (poderDePropulsion t2)
poderDePropulsion (Parte x t1 t2) = 0 + (poderDePropulsion t1) + (poderDePropulsion t2)


poderDePropulsion' :: Nave -> Int
poderDePropulsion' ParteBase = 0
poderDePropulsion' (Parte comp t1 t2) = componente comp + (poderDePropulsion t1) + (poderDePropulsion t2)

componente :: Componente -> Int
componente (Motor n) = n
componente _ = 0 

--1.4


desarmarse :: Nave -> Nave
desarmarse ParteBase = ParteBase
desarmarse (Parte x t1 t2) = if (esArma x)
								then Parte (cambiarArma x) (desarmarse t1)(desarmarse t2)
								else Parte x (desarmarse t1) (desarmarse t2) 

esArma :: Componente -> Bool
esArma Lanzatorpedos = True
esArma CanionLaser = True
esArma _ = False

cambiarArma :: Componente -> Componente
cambiarArma Lanzatorpedos = Escudo
cambiarArma CanionLaser = Escudo

--1.5


cantidadComida :: Nave -> Int
cantidadComida ParteBase = 0
cantidadComida (Parte (Almacen x) t1 t2) = aparicionesDeComidaEnAlmacen x + (cantidadComida t1) + (cantidadComida t2)
cantidadComida (Parte x t1 t2) = 0 + cantidadComida t1 + cantidadComida t2


aparicionesDeComidaEnAlmacen :: [Contenedor] -> Int
aparicionesDeComidaEnAlmacen [] = 0
aparicionesDeComidaEnAlmacen (x:xs) =  if (esComida x)
										then 1 + aparicionesDeComidaEnAlmacen xs
										else aparicionesDeComidaEnAlmacen xs

esComida :: Contenedor -> Bool
esComida Comida = True
esComida _ = False

--1.6


naveToTree :: Nave -> Tree Componente
naveToTree ParteBase = EmptyT
naveToTree (Parte x nave1 nave2) = NodeT x (naveToTree nave1)(naveToTree nave2)	

--1.7


aprovisionados :: [Contenedor] -> Nave -> Bool
aprovisionados xs ParteBase = True
aprovisionados xs (Parte x t1 t2) = (hayProvisionEnAlmacen xs x) && (aprovisionados xs t1) && (aprovisionados xs t2) 


hayProvisionEnAlmacen :: [Contenedor] -> Componente -> Bool
hayProvisionEnAlmacen cs (Almacen xs) = pertenecen cs xs
hayProvisionEnAlmacen cs _ = True

pertenecen :: [Contenedor] -> [Contenedor] -> Bool
pertenecen [] xs = True
pertenecen (c:cs) xs = (pertenece c xs) && (pertenecen cs xs)


pertenece :: Eq elemento => elemento -> [elemento] -> Bool
pertenece elemento [] = False
pertenece elemento (primero : resto ) = (primero == elemento) || pertenece elemento resto

--1.8

nave2 :: Nave
nave2 = (Parte (Motor 10) (Parte CanionLaser ParteBase ParteBase) (Parte (Motor 10) ParteBase ParteBase))

nave3 :: Nave
nave3 = Parte (Motor 5)
					(Parte (Almacen [Comida]) ParteBase ParteBase)
					(Parte (Almacen [Comida,Comida]) (Parte CanionLaser ParteBase ParteBase) 
						ParteBase)

armasNivelN :: Int -> Nave -> [Componente]
armasNivelN n ParteBase = [] 
armasNivelN n (Parte x t1 t2) =  if (esArma x && n==0) 
									then [x]
									else (armasNivelN (n-1) t1) ++ (armasNivelN (n-1) t2)
									
