data Organizador = MkO (Map Checksum (Set Persona)) (Map Persona (Set Checksum))

--Invariantes:
{-
1)No hay claves repetidas dentro del map 
2)Ninguna de las claves de los maps debe ser repetida
-}
--A
--1)
programasEnComun :: Persona -> Persona -> Organizador -> Set Checksum
programasEnComun p1 p2 orga = union(programasDe orga p1) (programasDe orga p2)

--2)
esUnGranHacker :: Organizador -> Persona -> Bool
esUnGranHacker orga p = poseenTodosLosElementos (set2list(programasDe orga p)) (todosLosProgramas o)  

poseenTodosLosElementos :: Eq a => [a] -> [a] -> Bool
poseenTodosLosElementos [] ys = True
poseenTodosLosElementos (x:xs) ys = (elem x ys) && (poseenTodosLosElementos xs ys)

--B
--A)
nuevo :: Organizador -- O(1)
nuevo = MkO (emptyM)(emptyM)

--B)
agregarPrograma :: Organizador -> Checksum -> Set Persona -> Organizador -- No se
agregarPrograma (MkO map1 map2) c setP = MkO (assocM c setP map1) (agregarProgramador c set2list(setP) map2)

agregarProgramador :: Checksum -> [a] -> Map k v -> Map k v
agregarProgramador c [] map = map
agregarProgramador c (x:xs) map = (assocM x c (agregarProgramador c xs map))

--C)
todosLosProgramas :: Organizador -> [Checksum] --O(n)
todosLosProgramas (MkO map1 map2) = domM map1

--D)
autoresDe :: Organizador -> Checksum -> Set Persona --O(log C)
autoresDe (MkO map1 map2) c = fromJust(lookupM map1 c)

--E)
programasDe :: Organizador -> Persona -> Set Checksum --O (log C)
programasDe (MkO map1 map2) p = fromJust(lookupM map2 p)

--F)
programaronJuntas :: Organizador -> Persona -> Persona -> Bool --O(logP + C log C)
programaronJuntas orga p1 p2 = isEmptyS(intersection (programasDe orga p1) (programasDe orga p2)) 

--G)
nroProgramasDePersona :: Organizador -> Persona -> Int
nroProgramasDePersona orga p = sizeS(programasDe orga p)

--H)

data Organizador = MkO (Map Checksum (Set Persona) Checksum Int) (Map Persona (Set Checksum))
--Checksum Int : representa el programa con mas autores


elMayorPrograma :: Organizador -> Maybe Checksum
elMayorPrograma (MkO map1 c n map2) = if (n == 0)
										then Nothing
										else Just c