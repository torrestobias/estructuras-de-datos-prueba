type Nombre = String
type Anio = Int
data Paper = P Nombre Anio deriving (Show,Eq)
data Grupo = Investigador Nombre [Paper] Grupo Grupo Grupo
			| Becario Nombre [Paper]
			| Vacio

type Conicet = [Grupo]


paper :: Paper
paper = (P "hola" 2000)

--a)
becarios :: Conicet -> [Nombre]
--Prop: dado un CONICET, denota la lista de nombres de los becarios en este
becarios [] = []
becarios (x:xs) = (nombresDeBecarios x) ++ becarios xs

nombresDeBecarios :: Grupo -> [Nombre]
nombresDeBecarios Vacio = []
nombresDeBecarios (Becario n papers) = [n]
nombresDeBecarios (Investigador n papers g1 g2 g3) = (nombresDeBecarios g1) ++ (nombresDeBecarios g2) ++ (nombresDeBecarios g3)


--b)
autores :: Paper -> Conicet -> [Nombre]
--Prop: dados un paper y un conicet, denota la lista de nombres de los investigadores o becarios en el conicet que son autores 
--del paper
autores p [] = []
autores p (x:xs) = (autoresDePapers p x) ++ (autores p xs)

autoresDePapers :: Paper -> Grupo -> [Nombre]
autoresDePapers p Vacio = []
autoresDePapers p (Becario n papers) = if (elem p papers)
										then [n]
										else []
autoresDePapers p (Investigador n papers g1 g2 g3) = if (elem p papers)
														then [n] ++ (autoresDePapers p g1) ++ (autoresDePapers p g2) ++ (autoresDePapers p g3)
														else (autoresDePapers p g1) ++ (autoresDePapers p g2) ++ (autoresDePapers p g3)

--c)
promoverBecarios :: [Nombre] -> Conicet -> Conicet
--Prop: dada una lista de nombres de becarios y un CONICET, denota el CONICET resultante de promover a todos los becarios en la
--lista a investigadores
promoverBecarios listaDeNombres [] = []
promoverBecarios listaDeNombres (x:xs) = (ascenderAInvestigador listaDeNombres x) : (promoverBecarios listaDeNombres xs)

ascenderAInvestigador :: [Nombre] -> Grupo -> Grupo
ascenderAInvestigador nombres Vacio = Vacio
ascenderAInvestigador nombres (Becario n papers) = if (elem n nombres)
													then (Investigador n papers Vacio Vacio Vacio)
													else (Becario n papers)
ascenderAInvestigador nombres (Investigador n papers g1 g2 g3) = (Investigador n papers (ascenderAInvestigador nombres g1) (ascenderAInvestigador nombres g2) (ascenderAInvestigador nombres g3))


--d)
sigloXXIConicet :: Conicet -> Conicet
--Prop: dado un CONICET, denota el CONICET resultante de quitar todos los papers que no hayan sido publicados en el sigloXXI
sigloXXIConicet [] = []
sigloXXIConicet (x:xs) = (sinPapers x) : (sigloXXIConicet xs)

sinPapers :: Grupo -> Grupo
sinPapers Vacio = Vacio
sinPapers (Becario n papers) = (Becario n (papersDelSigloXXI papers))
sinPapers (Investigador n papers g1 g2 g3) = (Investigador n (papersDelSigloXXI papers) (sinPapers g1) (sinPapers g2) (sinPapers g3))

papersDelSigloXXI :: [Paper] -> [Paper]
papersDelSigloXXI [] = []
papersDelSigloXXI (p:ps) = if (anioPaper p > 2000)
							then p : (papersDelSigloXXI ps)
							else papersDelSigloXXI ps 

anioPaper :: Paper -> Int
anioPaper (P n anio) = anio


--e)
--jefesDeBecario :: Nombre -> Grupo -> [Nombre]
--Prop: dados el nombre de un becario y un grupo, denota a todos los investigadores que se encuentran a su cargo, 
--directa o indirectamente
--Prec: hay un becario con dicho nombre en el grupo 
--Nota: no hay dos becarios con el mismo nombre