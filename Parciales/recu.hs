estaOrdenada :: Ord a => [a] -> Bool
estaOrdenada [x] = True
estaOrdenada (x:xs) = (x < (head xs)) && (estaOrdenada xs)