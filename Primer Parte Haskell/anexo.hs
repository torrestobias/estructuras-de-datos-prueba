prueba ::  [Int]
prueba = [1,2,33,4]

pares :: [(Int,Int)]
pares = [(1,2),(2,22),(90,0)]

headM :: [a] -> Maybe a
headM [] = Nothing 
headM xs = Just (head xs)

lastM :: [a] -> Maybe a
lastM [] = Nothing
lastM xs = Just (last xs)

maximumM :: Ord a => [a] -> Maybe a
maximumM [] = Nothing
maximumM xs = Just (maximum xs)

initM :: [a] -> Maybe [a]
initM [] = Nothing
initM xs = Just (init xs)


get :: Int -> [a] -> Maybe a
-- Dado un indice devuelve el elemento de esa posición.
get n [] = Nothing
get n xs = if (n > 0) 
			then get (n-1)(tail xs) 
			else headM xs 

indiceDe :: Eq a => a -> [a] -> Maybe Int
--Dado un elemento y una lista devuelve la posicion de la lista en la que se encuentra dicho elemento.
indiceDe e [] = Nothing					
indiceDe e xs = Just (indiceDe2 e xs)					

indiceDe2 :: Eq a => a -> [a] -> Int
indiceDe2 e (x:xs) = if (e /= x)
					then 1 + (indiceDe2 e xs)
					else (indiceDe2 e xs)


lookupM :: Eq k => [(k,v)] -> k -> Maybe v
lookupM [] k = Nothing
lookupM (x:xs) k = if ( (fst x) /= k)
					then lookupM xs k
					else Just (snd x)


fromJusts :: [Maybe a] -> [a]
--Devuelve los valores de los Maybe que no sean Nothing.
fromJusts ( Nothing: xs) = fromJusts xs
fromJusts [] = []
fromJusts ((Just x):xs) = x : (fromJusts xs)
				
maybes :: [Maybe Int]
maybes = [(Just 1),(Just 1),Nothing]


