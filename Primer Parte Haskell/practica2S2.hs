--1.1

data Dir = Norte | Este | Sur | Oeste


opuesto :: Dir -> Dir
opuesto Norte = Sur
opuesto Este = Oeste
opuesto Sur = Norte
opuesto Oeste = Este

siguiente :: Dir -> Dir
siguiente Norte = Este
siguiente Este = Sur
siguiente Sur = Oeste
siguiente Oeste = Norte

--2.2

data Persona = P String Int deriving (Show)

persona1 :: Persona
persona1 = P "Tobias" 23

persona2 :: Persona
persona2 = P "Jarvan IV" 40

nombre :: Persona -> String
nombre (P nombre edad) = nombre

edad :: Persona -> Int
edad (P nombre edad) = edad

crecer :: Persona -> Persona
crecer (P nombre edad) = P nombre (edad + 1)

cambioDeNombre :: String -> Persona -> Persona
cambioDeNombre nombreNuevo (P nombre edad) = P nombreNuevo edad

esMenorQueLaOtra :: Persona -> Persona -> Bool
esMenorQueLaOtra p1 p2 = edad p1 < edad p2

mayoresA :: Int -> [Persona] -> [Persona]
mayoresA edadAComprar [] = []
mayoresA edadAComprar (x:xs) = 	if ( edad x > edadAComprar)
								then x : mayoresA edadAComprar xs
								else mayoresA edadAComprar xs

promedio :: [Persona] -> Int
promedio [] = 0
promedio personas = div (length personas) (cantidadDePersonas personas)

cantidadDePersonas :: [Persona] -> Int
cantidadDePersonas [] = 0
cantidadDePersonas (x:xs) = 1 + cantidadDePersonas xs

elMasViejo :: [Persona] -> Persona
elMasViejo [p] = p
elMasViejo (x:xs) = esMayorQueElOtro x (elMasViejo xs)

esMayorQueElOtro :: Persona -> Persona -> Persona
esMayorQueElOtro p1 p2 = if edad p1 > edad p2
							then p1
							else p2

--2.3

data Pokemon = Poke TipoPokemon Int deriving (Show)
data TipoPokemon = Agua | Fuego | Planta  deriving (Eq,Show)
data Entrenador = Dt String [Pokemon] deriving (Show)

pikachu :: Pokemon
pikachu = Poke Agua 3

charizard :: Pokemon
charizard = Poke Fuego 4

bulbasour:: Pokemon
bulbasour = Poke Planta 4

dt :: Entrenador
dt = Dt "Pep" [pikachu,bulbasour]

lsTipos :: [TipoPokemon]
lsTipos = [Planta,Fuego,Agua]

leGanaAlToque :: Pokemon -> Pokemon -> Bool
leGanaAlToque p1 p2 = leGana (tipoDePokemon p1) (tipoDePokemon p2)

tipoDePokemon :: Pokemon -> TipoPokemon
tipoDePokemon (Poke tp n) = tp

leGana :: TipoPokemon -> TipoPokemon -> Bool
leGana Agua Fuego = True
leGana Fuego Planta = True
leGana Planta Agua = True
leGana _ _ = False

capturarPokemon :: Pokemon -> Entrenador -> Entrenador
capturarPokemon p (Dt nombre []) = (Dt nombre [p])
capturarPokemon p (Dt nombre pokemones) = (Dt nombre (p : pokemones))

cantidadDePokemons :: Entrenador -> Int
cantidadDePokemons (Dt nombre []) = 0
cantidadDePokemons (Dt nombre pokemons) = length pokemons

cantidadesDePokemonsDeTipo :: TipoPokemon -> Entrenador -> Int
cantidadesDePokemonsDeTipo unTipo (Dt nombre pokemons) = totalDePokemonsDeTipo unTipo pokemons

totalDePokemonsDeTipo :: TipoPokemon -> [Pokemon] -> Int
totalDePokemonsDeTipo unTipo [] = 0
totalDePokemonsDeTipo unTipo (p:ps) = if sonDelMismoTipo unTipo (tipoDePokemon p)
										then 1 + totalDePokemonsDeTipo unTipo ps
										else totalDePokemonsDeTipo unTipo ps

sonDelMismoTipo :: TipoPokemon -> TipoPokemon -> Bool
sonDelMismoTipo Agua Agua = True
sonDelMismoTipo Fuego Fuego = True
sonDelMismoTipo Planta Planta = True
sonDelMismoTipo _ _ = False

esExperto :: Entrenador -> Bool
esExperto (Dt nombre ps) = hayPokemonDeTipo Agua ps && hayPokemonDeTipo Fuego ps && hayPokemonDeTipo Planta ps

hayPokemonDeTipo :: TipoPokemon -> [Pokemon] -> Bool
hayPokemonDeTipo tp [] = False
hayPokemonDeTipo tp (p:ps) = sonDelMismoTipo tp (tipoDePokemon p) || hayPokemonDeTipo tp ps

data Pizza = PrePizza | Agregar Ingrediente Pizza deriving (Eq,Show)
data Ingrediente = Salsa | Queso | Jamon | AceitunasVerdes Int deriving (Eq, Show)


pizza = Agregar (AceitunasVerdes 9)
					(Agregar Jamon (
						Agregar Salsa(
							PrePizza )))

ingrediente :: Pizza -> [Ingrediente]
ingrediente PrePizza = []
ingrediente (Agregar i pizza) = i : (ingrediente pizza)

tieneJamon :: Pizza -> Bool
tieneJamon PrePizza = False
tieneJamon (Agregar i pizza) = esJamon i || (tieneJamon pizza)

sacarJamon :: Pizza -> Pizza
sacarJamon PrePizza = PrePizza
sacarJamon (Agregar ingrediente pizza) = if esJamon ingrediente
											then (sacarJamon pizza)
											else (Agregar ingrediente (sacarJamon pizza)) 

esJamon :: Ingrediente -> Bool
esJamon Jamon = True
esJamon _ = False

armarPizza :: [Ingrediente] -> Pizza
armarPizza [] = PrePizza
armarPizza (x:xs) = Agregar x (armarPizza xs)


duplicarAceitunas :: Pizza -> Pizza
duplicarAceitunas PrePizza = PrePizza
duplicarAceitunas (Agregar i pizza) = if esAceituna i
										then (Agregar (aumentarAceitunas i) (duplicarAceitunas pizza))
										else (duplicarAceitunas pizza)

esAceituna :: Ingrediente -> Bool
esAceituna (AceitunasVerdes n) = True
esAceituna _ = False

aumentarAceitunas :: Ingrediente -> Ingrediente
aumentarAceitunas (AceitunasVerdes n) = (AceitunasVerdes (n*2))

sacar :: [Ingrediente] -> Pizza -> Pizza
sacar [] pizza = pizza 
sacar (x:xs) pizza = sacarIngrediente x (sacar xs pizza)

{-
sacarIngrediente :: Ingrediente -> Pizza -> Pizza
sacarIngrediente ingrediente PrePizza = PrePizza
sacarIngrediente ingrediente (Agregar i pizza) = sacarSiEsIgual ingrediente i (sacarIngrediente ingrediente pizza)

agregarSiDiferente :: Ingrediente -> Ingrediente -> Pizza -> Pizza
agregarSiDiferente Queso Queso pizza = pizza
agregarSiDiferente Jamon Jamon pizza = pizza
agregarSiDiferente Salsa Salsa pizza = pizza
agregarSiDiferente (AceitunasVerdes _) (AceitunasVerdes _) = pizza
agregarSiDiferente _ ing pizza = (Agregar ing pizza) -}

sacarIngrediente :: Ingrediente -> Pizza -> Pizza
sacarIngrediente ingrediente PrePizza = PrePizza
sacarIngrediente ingrediente (Agregar i pizza) = if esIgualIngrediente ingrediente i
													then sacarIngrediente ingrediente pizza
													else (Agregar i) (sacarIngrediente ingrediente pizza)

esIgualIngrediente :: Ingrediente -> Ingrediente -> Bool
esIgualIngrediente Queso Queso = True
esIgualIngrediente Jamon Jamon = True
esIgualIngrediente Salsa Salsa = True
esIgualIngrediente (AceitunasVerdes _) (AceitunasVerdes _) = True
esIgualIngrediente _ _ = False

cantJamon :: [Pizza] -> [(Int,Pizza)]
cantJamon [] = []
cantJamon (p:pz) = (totalJamon p, p) : (cantJamon pz)

totalJamon :: Pizza -> Int
totalJamon PrePizza = 0
totalJamon (Agregar i pizza) = if (esJamon i)
								then 1 + (totalJamon pizza)
								else 0 + (totalJamon pizza)


mayorNAceitunas :: Int -> [Pizza] -> [Pizza]
mayorNAceitunas 0 [] = []
mayorNAceitunas n [] = []
mayorNAceitunas n (x:xs) = if (tieneMayorNAceitunas n x)
							then x : (mayorNAceitunas n xs)
							else mayorNAceitunas n xs

tieneMayorNAceitunas :: Int -> Pizza -> Bool
tieneMayorNAceitunas 0 PrePizza = False
tieneMayorNAceitunas 0 pizza = False
tieneMayorNAceitunas n (Agregar i pizza) = ( (esAceituna i) && (cantidadDeAceitunas i > n) ) || (tieneMayorNAceitunas n pizza)

cantidadDeAceitunas :: Ingrediente -> Int
cantidadDeAceitunas (AceitunasVerdes n) = n
cantidadDeAceitunas _ = 0



data Objeto = Cacharro | Tesoro deriving(Show,Eq)
data Camino = Fin | Cofre [Objeto] Camino | Nada Camino deriving (Show,Eq)

camino = Cofre [Cacharro,Tesoro]
				( Cofre [Cacharro,Cacharro]
					(Cofre [Cacharro,Cacharro]
						(Nada
							(Cofre [Cacharro]
								(Fin)))))


hayTesoro :: Camino -> Bool
hayTesoro Fin = False
hayTesoro (Nada camino) = (hayTesoro camino)
hayTesoro (Cofre obj camino) = (tieneTesoroEnCofre obj) || (hayTesoro camino)

tieneTesoroEnCofre :: [Objeto] -> Bool
tieneTesoroEnCofre [] = False
tieneTesoroEnCofre (x:xs) = (esTesoro x) || (tieneTesoroEnCofre xs)

esTesoro :: Objeto -> Bool
esTesoro Tesoro = True
esTesoro Cacharro = False


pasosHastaElTesoro :: Camino -> Int
pasosHastaElTesoro (Nada camino) = 1 + (pasosHastaElTesoro camino)
pasosHastaElTesoro (Cofre obj camino) = if ( tieneTesoroEnCofre obj)
											then 0
											else 1 + (pasosHastaElTesoro camino)

hayTesoroEn :: Int -> Camino -> Bool
hayTesoroEn n Fin = False
hayTesoroEn n (Nada camino) = (hayTesoroEn (n-1) camino)
hayTesoroEn n (Cofre obj camino) = if ( n > 0)
									then (hayTesoroEn (n-1) camino) 
									else tieneTesoroEnCofre obj
									
alMenosNTesoros :: Int -> Camino -> Bool
alMenosNTesoros n Fin = False
alMenosNTesoros n (Nada camino) = ( alMenosNTesoros (n-1) camino)
alMenosNTesoros n (Cofre obj camino) = 