data Tree a = EmptyT | NodeT a (Tree a)(Tree a) deriving (Show,Ord,Eq)

arbolSinHijoIzq :: Tree Int
arbolSinHijoIzq = NodeT 8 EmptyT
							(NodeT 10 EmptyT (NodeT 11 EmptyT EmptyT))

arbolSinHijoDer :: Tree Int
arbolSinHijoDer = NodeT 8 (NodeT 6 (NodeT 4 (NodeT 2 EmptyT EmptyT) EmptyT)
							(NodeT 7 EmptyT EmptyT))
							EmptyT

arbol1 :: Tree Int
arbol1 = NodeT 8 (NodeT 6 (NodeT 4 (NodeT 2 EmptyT EmptyT) EmptyT)
							(NodeT 7 EmptyT EmptyT))
				(NodeT 10 (NodeT 9 EmptyT EmptyT)
							(NodeT 11 EmptyT EmptyT))

arbolpar :: Tree (Int,Int)
arbolpar = NodeT (8,2) (NodeT (6,9) (NodeT (4,2) EmptyT EmptyT)
                                    (NodeT (7,20) EmptyT EmptyT))
                       (NodeT (10,5) (NodeT (9,60) EmptyT EmptyT)
                       	              (NodeT (11,689) EmptyT EmptyT))

perteneceBST :: Ord a => a -> Tree a -> Bool
perteneceBST a EmptyT = False
perteneceBST a (NodeT x ti td) = if ( a == x)
									then True
									else if (a < x)
										then perteneceBST a ti
										else perteneceBST a td

lookupBST :: Ord k => k -> Tree (k, v) -> Maybe v
lookupBST k EmptyT = Nothing
lookupBST k (NodeT x ti td) = if (k == (fst x))
								then Just (snd x)
								else if (k < (fst x))
										then lookupBST k ti
										else lookupBST k td

insertBST :: Ord a => a -> Tree a -> Tree a
insertBST a EmptyT = (NodeT a EmptyT EmptyT)
insertBST a (NodeT x ti td) = if (a == x)
							then NodeT x ti td
								else if a < x 
								then NodeT x (insertBST a ti) td
								else NodeT x ti (insertBST a td)

minBST :: Ord a => Tree a -> a
minBST (NodeT x EmptyT EmptyT) = x
minBST (NodeT x ti td) =  minBST ti

deleteMinBst :: Ord a => Tree a -> Tree a
deleteMinBst EmptyT = EmptyT
deleteMinBst (NodeT x EmptyT EmptyT) = EmptyT
deleteMinBst (NodeT x EmptyT td) = td 
deleteMinBst (NodeT x ti td) = NodeT x (deleteMinBst ti) td

maxBST :: Ord a => Tree a -> a
maxBST (NodeT x EmptyT EmptyT) = x
maxBST (NodeT x ti td) = maxBST td 

deleteMaxBst :: Ord a => Tree a -> Tree a
deleteMaxBst EmptyT = EmptyT
deleteMaxBst (NodeT x EmptyT EmptyT) = EmptyT
deleteMaxBst (NodeT x ti EmptyT) = ti
deleteMaxBst (NodeT x ti td) = NodeT x ti (deleteMaxBst td)

deleteBST :: Ord a => a -> Tree a -> Tree a
deleteBST a EmptyT = EmptyT 
deleteBST a (NodeT x EmptyT EmptyT) = if a == x
	                                  then EmptyT
	                                  else NodeT x EmptyT EmptyT
deleteBST a (NodeT x ti td) = if (a == x)
								then NodeT (maxBST ti) (deleteMaxBst ti) td
								else if a < x 
									then NodeT x (deleteBST a ti) td
									else NodeT x ti (deleteBST a td)

--splitMinBST :: Ord a => Tree a -> (a, Tree a)


