minimo :: Int -> Int -> Int
minimo n m = if n < m 
				then n 
				else m

sumatoria :: [Int] -> Int
sumatoria [] = 0
sumatoria (x:xs) = x + sumatoria xs

--1
sumaPar :: (Int,Int) -> Int
sumaPar (x,y) = x + y

andLogico :: Bool -> Bool -> Bool
andLogico True True = True
andLogico True _ = False

intercalar :: a -> [a] -> [a]
intercalar a [] = []
intercalar a (x:xs) = x : a : intercalar a xs

snoc :: [a] -> a -> [a]
snoc [] a = [a]
snoc (x:xs) a = x : snoc xs a

append :: [a] -> [a] -> [a]
append [] ys = ys
append (x:xs) ys = x : append xs ys

aplanar :: [[a]] -> [a]
aplanar [] = []
aplanar (x:xs) = x ++ aplanar xs


reversa :: [a] -> [a]
reversa [] = []
reversa (x:xs) =  (reversa xs) ++ [x]

zipMaximo :: [Int] -> [Int] -> [Int]
zipMaximo [] [] = []
zipMaximo (x:xs) [] = xs
zipMaximo [] (y:ys) = ys
zipMaximo (x:xs) (y:ys) = if x > y
							then x : zipMaximo xs ys
							else y: zipMaximo xs ys

zipSort :: [Int] -> [Int] -> [(Int,Int)]
zipSort [] [] = []
zipSort (x:xs)(y:ys) = if x < y
						then (x,y) : zipSort xs ys
						else (y,x) : zipSort xs ys

{-
promedio :: [Int] -> Int
promedio [] = 0
promedio xs = (sumatoria xs) div (length xs) -}

--2.2

minimum' :: Ord a => [a] -> a
minimum' [x] = x
minimum' (x:xs) = min x (minimum' xs)


factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n-1)

cuentaRegresiva :: Int -> [Int]
cuentaRegresiva 0 = []
cuentaRegresiva n = n : cuentaRegresiva (n-1)

contarHasta :: Int -> [Int]
contarHasta 0 = []
contarHasta n = contarHasta (n-1) ++ [n]

replicarN :: Int -> a -> [a]
replicarN 0 a = []
replicarN n a = a : replicarN (n-1) a

desdeHasta :: Int -> Int -> [Int]
desdeHasta n m = if n <= m
					then n : desdeHasta (n+1) m
					else [] 

takeN :: Int -> [a] -> [a]
takeN n [] = []
takeN 0 xs = []
takeN 0 [] = []
takeN n (x:xs) = x : takeN (n-1) xs

maximum' :: Ord a => [a] -> a
maximum' [a] = a
maximum' (x:xs) = max x (maximum' xs)



particionPorSigno :: [Int] -> ([Int],[Int])
particionPorSigno [] = ([],[])
particionPorSigno (x:xs) = ponerDentro x (particionPorSigno xs)

ponerDentro :: Int -> ([Int],[Int]) -> ([Int],[Int])
ponerDentro x (ps,ns) = if x > 0
						then (x:ps,ns)
						else (ps, x:ns)




esPrefijo :: Eq a => [a] -> [a] -> Bool
esPrefijo (x:xs) [] = False
esPrefijo [] ys = True
esPrefijo (x:xs) (y:ys) = if y == x
							then esPrefijo xs ys
							else False

agregarAlGrupo :: Eq a => a -> [[a]] -> [[a]]
agregarAlGrupo a [] = [[a]]
agregarAlGrupo a (xs:xss) = if (elem a xs)
							then (a:xs): xss
							else [a] : (xs:xss)