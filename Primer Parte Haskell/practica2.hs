--Practica 2

--1

data Dir = Norte | Sur | Este | Oeste deriving (Eq,Show)

opuesto :: Dir -> Dir
opuesto Norte = Sur
opuesto Sur = Norte
opuesto Este = Oeste
opuesto Oeste = Este

siguiente :: Dir -> Dir 
siguiente Norte = Este
siguiente Este = Sur
siguiente Sur = Oeste
siguiente Oeste = Norte

promedio :: [Int] -> Int
promedio [] = 0
promedio (x:xs) = div (sumatoria (x:xs)) (longitud (x:xs))


sumatoria :: [Int] -> Int
sumatoria [] = 0
sumatoria (x : xs) = x + sumatoria xs

--2.2

longitud :: [a] -> Int
longitud [] = 0
longitud (x : xs) = 1 + longitud xs

--2

data Persona = Person String Int deriving (Show)

nombre :: Persona -> String
nombre (Person nombre edad) = nombre

edad :: Persona -> Int
edad (Person nombre edad) = edad

crecer :: Persona -> Persona
crecer (Person nombre edad) = Person nombre(edad + 1)

cambioDeNombre :: String -> Persona -> Persona
cambioDeNombre nuevoNombre (Person nombre edad) = Person nuevoNombre edad

esMenorQueLaOtra :: Persona -> Persona -> Bool
esMenorQueLaOtra (Person nombre1 edad1) (Person nombre2 edad2) = edad1 < edad2

mayoresA :: Int -> [Persona] -> [Persona]
mayoresA edadLimite [] = []
mayoresA edadLimite (primerPersona : resto) = if edadLimite < (edad primerPersona) 
												then primerPersona : mayoresA edadLimite resto
												else mayoresA edadLimite resto

promedioEdad :: [Persona] -> Int
promedioEdad (x:xs) = (promedio (edades xs))

pertenece :: Eq elemento => elemento -> [elemento] -> Bool
pertenece elemento [] = False
pertenece elemento (primero : resto ) = (primero == elemento) || pertenece elemento resto

edades :: [Persona] -> [Int]
edades [] = [] 
edades (x:xs) = edad x : edades xs


elMasViejo :: [Persona] -> Persona
elMasViejo [a] = a
elMasViejo (x:xs) = elMayor x (elMasViejo xs)

elMayor :: Persona -> Persona -> Persona
elMayor (Person nombre1 edad1 ) (Person nombre2 edad2) = if edad1 > edad2
															then Person nombre1 edad1
															else Person nombre2 edad2

--2.2.3

data Pokemon = Poke TipoDePokemon Int deriving (Show)
data TipoDePokemon = Agua | Fuego | Planta deriving (Eq,Show)
data Entrenador = Dt String [Pokemon] deriving (Show)

tipo :: Pokemon -> TipoDePokemon
tipo (Poke tipoDePoke energy) = tipoDePoke

nombreDt :: Entrenador -> String
nombreDt (Dt nombreTecnico pokemon) = nombreTecnico

porcentajeDeEnergia :: Pokemon -> Int
porcentajeDeEnergia (Poke tipoDePoke energy) = energy 

leGana :: TipoDePokemon -> TipoDePokemon -> Bool
leGana Agua Fuego = True
leGana Fuego Planta = True
leGana Planta Agua = True
leGana _ _ = False

leGanaAlToque :: Pokemon -> Pokemon -> Bool
leGanaAlToque (Poke tipoDePoke1 energy1) (Poke tipoDePoke2 energy2) = leGana tipoDePoke1 tipoDePoke2

capturarPokemon :: Pokemon -> Entrenador -> Entrenador
capturarPokemon (Poke tipoDePoke energy) (Dt nombreTecnico pokes) = (Dt nombreTecnico ((Poke tipoDePoke energy) : pokes))

cantidadDePokemons :: Entrenador -> Int
cantidadDePokemons (Dt nombreTecnico listaPokemons) = longitud listaPokemons

cantidadDePokemonsDeTipo :: TipoDePokemon -> Entrenador -> Int
cantidadDePokemonsDeTipo tipoDePoke (Dt ndt []) = 0
cantidadDePokemonsDeTipo tipoDePoke (Dt ndt (poke:pokes)) = if tipoDePoke == tipo (poke) 
															then 1 + cantidadDePokemonsDeTipo tipoDePoke (Dt ndt pokes) 
															else cantidadDePokemonsDeTipo tipoDePoke (Dt ndt pokes)

esExperto :: Entrenador -> Bool
esExperto (Dt ndt []) = False
esExperto entrenador = cantidadDePokemonsDeTipo Agua entrenador >= 1 && 
						cantidadDePokemonsDeTipo Fuego entrenador >= 1 &&
						cantidadDePokemonsDeTipo Planta entrenador >= 1

--2.2.4

data Pizza = Prepizza | Agregar Ingrediente Pizza deriving (Show)

data Ingrediente = Salsa | Queso | Jamon | AceitunasVerdes Int deriving (Show,Eq)

ingredientes :: Pizza -> [Ingrediente]
ingredientes Prepizza = []
ingredientes (Agregar ingrediente pizza) = ingrediente : ingredientes pizza

tieneJamon :: Pizza -> Bool
tieneJamon Prepizza = False
tieneJamon (Agregar ingrediente pizza) = ingrediente == Jamon || tieneJamon pizza

sacarJamon :: Pizza -> Pizza
sacarJamon Prepizza = Prepizza
sacarJamon (Agregar Jamon pizza) = sacarJamon pizza
sacarJamon (Agregar ingrediente pizza) = Agregar ingrediente (sacarJamon pizza)

armarPizza :: [Ingrediente] -> Pizza
armarPizza [] = Prepizza
armarPizza (ingrediente:pizza) = Agregar ingrediente (armarPizza pizza)

duplicarAceitunas :: Pizza -> Pizza
duplicarAceitunas Prepizza = Prepizza
duplicarAceitunas (Agregar (AceitunasVerdes n) pizza) = Agregar (AceitunasVerdes (n*2))(duplicarAceitunas pizza)
duplicarAceitunas (Agregar ingrediente pizza) = Agregar ingrediente (duplicarAceitunas pizza)

{-
sacar :: [Ingrediente] -> Pizza -> Pizza
sacar [] Prepizza = Prepizza
sacar ingredientesASacar ( x :pizza) = if (pertenece x ingredientesASacar)
										then sacar pizza
										else [x] ++ (sacar pizza )
-}


-----------------------------------------------------------------------------------------

data Objeto = Cacharro | Tesoro deriving (Eq,Show)
data Camino = Fin | Cofre [Objeto] Camino | Nada Camino deriving(Show)

caminoConTesoro :: Camino
caminoConTesoro = Nada (Cofre [Tesoro] Fin)

caminoIziPissi :: Camino
caminoIziPissi = Nada (Cofre []( Cofre [Cacharro] (Nada Fin) ) )

hayTesoro :: Camino -> Bool
hayTesoro Fin = False
hayTesoro (Cofre objetos camino) = (tieneTesoro objetos)  || (hayTesoro camino)
hayTesoro (Nada camino) = hayTesoro camino 


tieneTesoro :: [Objeto] -> Bool
tieneTesoro [] = False
tieneTesoro (x:xs) = (x == Tesoro) || tieneTesoro xs 


