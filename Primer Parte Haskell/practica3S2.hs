data Tree a = EmptyT | NodeT a (Tree a)(Tree a) deriving (Show,Eq)

arbol1 :: Tree Int
arbol1 = NodeT 3 (NodeT 5 EmptyT EmptyT)
					(NodeT 30 EmptyT EmptyT)

arbol2 :: Tree Int
arbol2 = NodeT 3 ( NodeT 5 EmptyT EmptyT)
                  ( NodeT 2 (NodeT 1 EmptyT EmptyT)
                          EmptyT)					

countLeaves :: Tree a -> Int
countLeaves EmptyT = 0
countLeaves (NodeT x EmptyT EmptyT) = 1
countLeaves (NodeT x i d) = (countLeaves i) + (countLeaves d)

esHoja :: Tree a -> Bool
esHoja EmptyT = False
esHoja (NodeT x i d) = (isEmpty i) && (isEmpty d)

isEmpty :: Tree a -> Bool
isEmpty EmptyT = True
isEmpty _ = False

leaves :: Tree a -> [a]
leaves EmptyT = []
leaves (NodeT x i d) = if esHoja( NodeT x i d)
						then [x] ++ (leaves i) ++ (leaves d)
						else (leaves i) ++ (leaves d)

heigthT :: Tree a -> Int
heigthT EmptyT = 0
heigthT (NodeT x i d) = 1 + max (heigthT i)(heigthT d)

countNotLeaves :: Tree a -> Int
countNotLeaves EmptyT = 0
countNotLeaves (NodeT x EmptyT EmptyT) = 0
countNotLeaves (NodeT x i d) = 1 + (countNotLeaves i) + (countNotLeaves d)

mirrorT :: Tree a -> Tree a
mirrorT EmptyT = EmptyT
mirrorT (NodeT x i d) = ( NodeT x d i)

listInOrder :: Tree a -> [a]
listInOrder EmptyT = []
listInOrder (NodeT x i d) = (listInOrder i) ++ [x] ++ (listInOrder d)

listPreOrder :: Tree a -> [a]
listPreOrder EmptyT = []
listPreOrder (NodeT x i d) = [x] ++ (listPreOrder i) ++ (listPreOrder d)

listPosOrder :: Tree a -> [a]
listPosOrder EmptyT = []
listPosOrder (NodeT x i d) = (listPosOrder i) ++ (listPosOrder d) ++ [x]

concatenarListasT :: Tree [a] -> [a]
concatenarListasT EmptyT = []
concatenarListasT (NodeT x i d) = (concatenarListasT i) ++ x ++ (concatenarListasT d)

levelN :: Int -> Tree a -> [a]
levelN n EmptyT = []
levelN n (NodeT x i d) = if ( n > 0)
							then (levelN (n-1) i) ++ (levelN (n-1) d)
							else [x]

listPerLevel :: Tree a -> [[a]]
listPerLevel EmptyT = []
listPerLevel (NodeT x i d) = [x] :  unirListas (listPerLevel i)(listPerLevel d)

unirListas :: [[a]] -> [[a]] -> [[a]]
unirListas xs [] = xs
unirListas [] ys = ys
unirListas (xs:xss)(ys:yss) = (xs ++ ys) : (unirListas xss yss)

ramaMasLarga :: Tree a -> [a]
ramaMasLarga EmptyT = []
ramaMasLarga (NodeT x i d) = x : laRamaMasLarga (ramaMasLarga i )(ramaMasLarga d)

laRamaMasLarga :: [a] -> [a] -> [a]
laRamaMasLarga xs ys = if ( length xs > length ys)
						then xs
						else ys

todosLosCaminos :: Tree a -> [[a]]
todosLosCaminos EmptyT = []
todosLosCaminos (NodeT x EmptyT EmptyT) = [[x]]
todosLosCaminos (NodeT x i d) = agregarATodas x ( (todosLosCaminos i) ++ (todosLosCaminos d))

agregarATodas :: a -> [[a]] -> [[a]]
agregarATodas x [] = []
agregarATodas x (ys:yss) = (x:ys) : (agregarATodas x yss)


-------------------------------------------------------------------------------------------------------------

data Dir = Izq | Der deriving (Show)
data Objeto = Tesoro | Chatarra deriving (Show)
data Mapa = Cofre Objeto | Bifurcacion Objeto Mapa Mapa deriving(Show)


camino :: Mapa
camino = Bifurcacion Chatarra
							(Bifurcacion Chatarra (Cofre Chatarra)(Cofre Tesoro))
							(Cofre Tesoro)
camino2 :: Mapa
camino2 = Bifurcacion Chatarra
							(Bifurcacion Tesoro (Cofre Chatarra)(Cofre Chatarra))
							(Bifurcacion Chatarra (Cofre Chatarra)(Cofre Tesoro))
hayTesoro :: Mapa -> Bool
hayTesoro (Cofre o) = (esTesoro o)
hayTesoro (Bifurcacion o m1 m2) = (esTesoro o) || ((hayTesoro m1) || (hayTesoro m2))

esTesoro :: Objeto -> Bool
esTesoro Tesoro = True
esTesoro _ = False

hayTesoroEn :: [Dir] -> Mapa -> Bool
hayTesoroEn [] (Cofre o) = (esTesoro o)
hayTesoroEn (d:ds) (Bifurcacion o m1 m2) = if (esIzq d)
											then (hayTesoroEn ds m1)
											else (hayTesoroEn ds m2)

esIzq :: Dir -> Bool
esIzq Izq = True
esIzq Der = False

caminoAlTesoro :: Mapa -> [Dir]
caminoAlTesoro (Cofre Tesoro) = []
caminoAlTesoro (Bifurcacion o m1 m2) = if hayTesoro m1
										then Izq : (caminoAlTesoro m1)
										else Der : (caminoAlTesoro m2)



caminoRamaMasLarga :: Mapa -> [Dir]
caminoRamaMasLarga (Cofre o) = []
caminoRamaMasLarga (Bifurcacion o m1 m2) =	if largoDelMapa m1 > largoDelMapa m2
												then Izq : (caminoRamaMasLarga m1)
												else Der : (caminoRamaMasLarga m2)

largoDelMapa :: Mapa -> Int
largoDelMapa (Cofre o) = 0
largoDelMapa (Bifurcacion o m1 m2) = 1 + (max (largoDelMapa m1)(largoDelMapa m2))


tesorosPerLevel :: Mapa -> [[Objeto]] 
tesorosPerLevel (Cofre Chatarra) = []
tesorosPerLevel (Cofre Tesoro) = [[Tesoro]]
tesorosPerLevel (Bifurcacion o m1 m2) = if (esTesoro o)
										then [o] : unirListas (tesorosPerLevel m1)(tesorosPerLevel m2)
										else unirListas (tesorosPerLevel m1) (tesorosPerLevel m2)

todosLosCaminosDos :: Mapa -> [[Dir]]
todosLosCaminosDos (Cofre o) = []
todosLosCaminosDos (Bifurcacion o m1 m2) = 	 ((todosLosCaminosDos m1) ++ (todosLosCaminosDos m2))

