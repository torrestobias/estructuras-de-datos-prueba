data Arbol a = Vacio | Nodo a (Arbol a)(Arbol a) deriving(Show,Eq)

data Dir = Izq | Der deriving(Show,Eq)

data Celda = ConsCelda Int Int deriving(Show,Eq)

arbol:: Arbol Celda
arbol = Nodo (ConsCelda 2 3)
				(Nodo (ConsCelda 1 2) Vacio Vacio)
				(Nodo (ConsCelda 1 50) Vacio Vacio)

existeCamino :: [Dir] -> Arbol Celda -> Bool
existeCamino ds Vacio = False
existeCamino [] _ = True
existeCamino (Izq:ds) (Nodo x ti td) = (existeCamino ds ti)
existeCamino (Der:ds) (Nodo x ti td) = (existeCamino ds td)

rojasDeCelda :: [Dir] -> Arbol Celda -> Int
rojasDeCelda ds Vacio = 0
rojasDeCelda [] (Nodo x i d) = (cantidadRojas x)
rojasDeCelda (Izq:ds) (Nodo x ti td) = (cantidadRojas x) + (rojasDeCelda ds ti)
rojasDeCelda (Der:ds) (Nodo x ti td) = (cantidadRojas x) + (rojasDeCelda ds td)

cantidadRojas :: Celda -> Int
cantidadRojas (ConsCelda v r) = r


celdaConMasRojas :: Arbol Celda -> Celda
celdaConMasRojas Vacio = (ConsCelda 0 0)
celdaConMasRojas (Nodo x i d) = tieneMasRojas x (tieneMasRojas(celdaConMasRojas i) (celdaConMasRojas d))

tieneMasRojas :: Celda -> Celda -> Celda
tieneMasRojas c1 c2 = if ( (cantidadRojas c1) > (cantidadRojas c2))
						then c1
						else c2

vaciarCeldas :: [[Dir]] -> Arbol Celda -> Arbol Celda
vaciarCeldas ds Vacio = Vacio
vaciarCeldas [] (Nodo x i d) = (Nodo (ConsCelda 0 0) i d)
vaciarCeldas (ds:dss) (Nodo x i d) = if (existeCamino ds i)
	                            then (vaciarCeldas dss i)
	                            else (vaciarCeldas dss d)

