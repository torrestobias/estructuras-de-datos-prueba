a :: Int
a = 1

f :: (a, (b, c)) -> ((a,b ),c )
f (x, (y,z)) = ((x,y),z)

minimo :: Int -> Int -> Int
minimo a b = if a < b then a else b

minimo3 :: Int -> Int -> Int -> Int
minimo3 a b c = if minimo a b < c then minimo a b else c 




--Practica 1 --

--1.A

siguiente :: Int -> Int
siguiente a = a + 1

sucesor :: Int -> Int
sucesor a = siguiente a

--1.B

sumar :: Int -> Int -> Int
sumar a b = a + b

--1.C

maximo :: Int -> Int -> Int
maximo a b = if a > b then a else b

--2.A

negar' :: Bool -> Bool
negar' a = if (a == True) then False else True


negar :: Bool -> Bool
negar True = False
negar _ = True

--2.B

andLogico :: Bool -> Bool -> Bool
andLogico a b = if (a == True ) then b else False

andLogico' :: Bool -> Bool -> Bool
andLogico' a True = True
andLogico' _ False = False

--2.C

orLogico :: Bool -> Bool -> Bool
orLogico a False = a 
orLogico _ True = True

--2.D

primera :: (Int,Int) -> Int
primera (a,b) = a

segunda :: (Int,Int) -> Int
segunda (a,b) = b

--2.E

sumarPar :: (Int,Int) -> Int
sumarPar (a,b) = a + b

--2.G

maxDelPar :: (Int,Int) -> Int
maxDelPar (a,b) = maximo a b

--3.A

loMismo :: a -> a
loMismo a = a

--3.B

siempreSiete :: a -> Int
siempreSiete a = 7

--3.C

duplicar :: a -> (a,a)
duplicar a = (a,a)

--3.D

singleton :: a -> [a]
singleton a = [a]

--4.A

isEmpty :: [a] -> Bool
isEmpty [a] = False
isEmpty _ = True

--4.B

head' :: [a] -> a
head' (x : xs) = x


--4.C

tail' :: [a] -> [a]
tail' (x : xs) = xs

--Recursion sobre listas

--2.1

sumatoria :: [Int] -> Int
sumatoria [] = 0
sumatoria (x : xs) = x + sumatoria xs

--2.2

longitud :: [a] -> Int
longitud [] = 0
longitud (x : xs) = 1 + longitud xs

--2.3

mapSucesor :: [Int] -> [Int]
mapSucesor [] = []
mapSucesor (x : xs) = (x+1) : mapSucesor xs 

--2.4

mapSumaPar :: [(Int,Int)] -> [Int]
mapSumaPar [] = []
mapSumaPar ( primero : resto) = ( sumarPar primero) : mapSumaPar resto 

--2.5

mapMaxDelPar :: [(Int,Int)] -> [Int]
mapMaxDelPar [] = []
mapMaxDelPar (primero : resto) = (maxDelPar primero) : mapMaxDelPar resto


--2.6

todoVerdad :: [Bool] -> Bool
todoVerdad [] = True
todoVerdad ( primero : resto) = primero && todoVerdad resto

--2.7

algunaVerdad :: [Bool] -> Bool
algunaVerdad [] = False
algunaVerdad (primero : resto) = primero || algunaVerdad resto

--2.8

pertenece :: Eq elemento => elemento -> [elemento] -> Bool
pertenece elemento [] = False
pertenece elemento (primero : resto ) = (primero == elemento) || pertenece elemento resto

--2.9

apariciones :: Eq elemento => elemento -> [elemento] -> Int
apariciones elemento [] = 0
apariciones elemento (primero : resto) = if  elemento == primero 
										then 1 + apariciones elemento resto  
										else 0 + apariciones elemento resto

--2.10

filtrarMenoresA :: Int -> [Int] -> [Int]
filtrarMenoresA a [] = []
filtrarMenoresA a (primero:resto) = if primero < a 
									then primero : filtrarMenoresA a resto 
									else filtrarMenoresA a resto

--2.11

filtrarElemento :: Eq a => a -> [a] -> [a]
filtrarElemento a [] = []
filtrarElemento a (primero:resto) = if a == primero 
									then filtrarElemento a resto 
									else primero : filtrarElemento a resto

--2.12

mapLongitudes :: [[a]] -> [Int]
mapLongitudes [] = []
mapLongitudes (x:xs) = 	longitud x : mapLongitudes xs

--2.13

longitudMayorA :: Int -> [[a]] -> [[a]]
longitudMayorA n [] = []
longitudMayorA n (x : xs) = if (longitud x > n) 
							then x : longitudMayorA n xs 
							else longitudMayorA n xs 

--2.14

intercalar :: a -> [a] -> [a]
intercalar a [] = []
intercalar a [b] = [b]
intercalar a (x:xs) =  x : a : intercalar a xs 

--2.15

snoc :: [a] -> a -> [a]
snoc [] a = []
snoc (x:xs) a = (x:xs) ++ [a]

--AVISALE A FRANCISCO QUE TIENE EL CELULAR CARGANDO

--2.16

append :: [a] -> [a] -> [a]
append [] ys = ys
append (x:xs) ys = x : append xs ys

--2.17

aplanar :: [[a]] -> [a]
aplanar [] = []
aplanar (x:xs) = x ++ aplanar xs

--2.18

reversa :: [a] -> [a]
reversa [] = []
reversa (x :xs) =  reversa xs ++ [x]

--2.19

zipMaximos :: [Int] -> [Int] -> [Int]
zipMaximos [] ys = []
zipMaximos xs [] = []
zipMaximos (x:xs) (y:ys) = max x y : zipMaximos xs ys

--2.20

zipSort :: [Int] -> [Int] -> [(Int,Int)]
zipSort [] ys = []
zipSort (x:xs) (y:ys) = if  x < y 
						then (x,y) : zipSort xs ys 
						else (y,x) :zipSort xs ys

--2.21

promedio :: [Int] -> Int
promedio [] = 0
promedio (x:xs) = div (sumatoria (x:xs)) (longitud (x:xs))

--2.22

minimun :: Ord a => [a] -> a
minimun [x] = x
minimun (x:xs) = min x (minimun xs) 

-- Recursion sobre numeros

--2.2.1

factorial :: Int -> Int
factorial 0 = 1
factorial n =  n * factorial (n-1)

--2.2.2

cuentaRegresiva :: Int -> [Int]
cuentaRegresiva 0 = []
cuentaRegresiva n = n :	cuentaRegresiva (n -1)

--2.2.3

contarHasta :: Int -> [Int]
contarHasta 0 = []
contarHasta n = contarHasta (n - 1) ++ [n]

--2.2.4

replicarN :: Int -> a -> [a]
replicarN 0 a = []
replicarN n a =  a : replicarN (n - 1) a

--2.2.5
-- gracias ericrack <3
desdeHasta :: Int -> Int -> [Int]
desdeHasta n m = if ( n == m )
					then [m]
					else n : desdeHasta (n + 1) m

--2.2.6
takeN :: Int -> [a] -> [a]
takeN n [] = []
takeN n (x:xs) = if ( n > 0)
					then x : takeN (n-1) xs 
					else takeN n xs
					