longitud :: [a] -> Int
longitud [] = 0
longitud (x : xs) = 1 + longitud xs

data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving (Show)

arbol1 :: Tree Int
arbol1 = NodeT 31 ( NodeT 14 EmptyT (NodeT 30 EmptyT EmptyT) ) (NodeT 20 EmptyT EmptyT)

arbol2 :: Tree Int
arbol2 = NodeT 20 ( NodeT 15 EmptyT	(NodeT 20 EmptyT EmptyT)) (NodeT 15 EmptyT (NodeT 10 EmptyT EmptyT))

arbol3 :: Tree String
arbol3 = NodeT "hola" ( NodeT "si" EmptyT (NodeT "casa" EmptyT EmptyT) ) (NodeT "paises" EmptyT EmptyT)

--1.1

sumarT :: Tree  Int -> Int
sumarT EmptyT = 0
sumarT (NodeT x ti td) = x + sumarT ti + sumarT td

---------------------------------------------------------------------------------------

--1.2
	
sizeT :: Tree a -> Int
sizeT EmptyT = 0
sizeT ( NodeT x ti td) = 1 + sizeT ti + sizeT td

---------------------------------------------------------------------------------------

--1.3

mapDobleT :: Tree Int -> Tree Int
mapDobleT EmptyT = EmptyT
mapDobleT (NodeT x ti td) = NodeT (x * 2) (mapDobleT ti) (mapDobleT td)

---------------------------------------------------------------------------------------

--1.4

mapLongitudT :: Tree String -> Tree Int
mapLongitudT EmptyT = EmptyT
mapLongitudT (NodeT x ti td) = NodeT (longitud x) (mapLongitudT ti) (mapLongitudT td)

---------------------------------------------------------------------------------------

--1.5

perteneceT :: Eq a => a -> Tree a -> Bool
perteneceT a EmptyT = False
perteneceT a (NodeT x ti td) = (x == a) || (perteneceT a ti) || (perteneceT a td)

---------------------------------------------------------------------------------------

--1.6

aparicionesT :: Eq a => a -> Tree a -> Int
aparicionesT a EmptyT = 0
aparicionesT a (NodeT x ti td) =  if (x == a) 
									then 1 + ( aparicionesT a ti) + (aparicionesT a td) 
									else 0 + ( aparicionesT a ti ) +(aparicionesT a td)

---------------------------------------------------------------------------------------

--1.7

countLeaves :: Tree a -> Int
countLeaves EmptyT = 0
countLeaves (NodeT x ti td) = if  esHoja (NodeT x ti td)
								then 1 
								else countLeaves ti + countLeaves td

esHoja :: Tree a -> Bool
esHoja EmptyT = False
esHoja (NodeT x ti td) = isEmpty ti && isEmpty td

isEmpty :: Tree a -> Bool
isEmpty EmptyT = True
isEmpty _ = False

---------------------------------------------------------------------------------------

--1.8

leaves :: Tree a -> [a]
leaves EmptyT = []
leaves (NodeT x ti td ) = if esHoja (NodeT x ti td)
							then [x]
							else leaves ti ++ leaves td 

---------------------------------------------------------------------------------------

--1.9

heightT :: Tree a -> Int
heightT EmptyT = 0
heightT (NodeT x ti td) =  1 + (max ( heightT ti)(heightT td))

---------------------------------------------------------------------------------------

--1.10

countNotLeaves :: Tree a -> Int
countNotLeaves EmptyT = 0
countNotLeaves (NodeT x ti td) = if not( esHoja (NodeT x ti td))
									then 1 + countNotLeaves ti + countNotLeaves td

									else countNotLeaves ti + countNotLeaves td 

---------------------------------------------------------------------------------------

--1.11

mirrorT :: Tree a -> Tree a
mirrorT EmptyT = EmptyT
mirrorT (NodeT x ti td) = NodeT x (mirrorT td) (mirrorT ti)

---------------------------------------------------------------------------------------

--1.12

listInOrder :: Tree a -> [a]
listInOrder EmptyT = []
listInOrder (NodeT x ti td) = (listInOrder ti) ++ [x] ++ (listInOrder td) 

---------------------------------------------------------------------------------------

--1.13

listPreOrder :: Tree a -> [a]
listPreOrder EmptyT = []
listPreOrder (NodeT x ti td) = [x] ++ (listPreOrder ti) ++ (listPreOrder td)

---------------------------------------------------------------------------------------

--1.14

listPostOrder :: Tree a -> [a]
listPostOrder EmptyT = []
listPostOrder (NodeT x ti td) = (listPostOrder ti) ++ (listPostOrder td) ++ [x]

---------------------------------------------------------------------------------------

--1.15

arbol4 :: Tree [Int] 
arbol4 = NodeT [11,12] (NodeT  [3,4,5] EmptyT EmptyT ) (NodeT [20,21,22] EmptyT EmptyT)

concatenarListasT :: Tree [a] -> [a]
concatenarListasT EmptyT = []
concatenarListasT (NodeT xs ti td) = concatenarListasT ti ++ xs ++ concatenarListasT td

---------------------------------------------------------------------------------------

--1.16

levelN :: Int -> Tree a -> [a]
levelN n EmptyT = []
levelN n (NodeT x ti td) = if ( n == 0) 
								then  []
								else  (levelN (n-1) ti ) ++ (levelN (n-1) td)

---------------------------------------------------------------------------------------

--1.17

listPerLevel :: Tree a -> [[a]]
listPerLevel EmptyT = []
listPerLevel (NodeT x ti td) = [x] : juntarNiveles (listPerLevel ti)( listPerLevel td)

juntarNiveles :: [[a]] -> [[a]] -> [[a]]
juntarNiveles xss [] = xss
juntarNiveles [] yss = yss
juntarNiveles (xs : xss) (ys: yss) = (xs ++ ys) : juntarNiveles xss yss

---------------------------------------------------------------------------------------

--1.18


ramaMasLarga :: Tree a -> [a]
ramaMasLarga EmptyT = []
ramaMasLarga (NodeT x ti td) = x : masLarga (ramaMasLarga ti) (ramaMasLarga td) 

masLarga :: [a] -> [a] -> [a]
masLarga xs [] = xs
masLarga [] ys = ys
masLarga (x : xs) (y : ys) = if ( longitud xs > longitud ys)
								then x : xs
								else y :ys	


							
---------------------------------------------------------------------------------------

--1.19

todosLosCaminos:: Tree a -> [[a]]
todosLosCaminos EmptyT = []
todosLosCaminos (NodeT x EmptyT EmptyT) = [[x]]
todosLosCaminos (NodeT x ti td) = agregarATodes x (todosLosCaminos ti ++ todosLosCaminos td)

agregarATodes :: a -> [[a]] -> [[a]]
agregarATodes x [] = []
agregarATodes x (xs: xss) = (x:xs) : agregarATodes x xss

---------------------------------------------------------------------------------------

data Dir = Izq | Der deriving(Eq, Show)

data Objeto = Tesoro | Chatarra deriving(Eq,Show)

data Mapa = Cofre Objeto | Bifurcacion Objeto Mapa Mapa deriving (Eq,Show)

--data Mapa = Cofre [Objeto] | Bifurcacion [Objeto] Mapa Mapa deriving (Show)

mapa1 :: Mapa
mapa1 = Bifurcacion Chatarra 
					(Bifurcacion Chatarra (Cofre Chatarra) (Cofre Chatarra)) 
					(Bifurcacion Chatarra (Cofre Chatarra) (Bifurcacion Chatarra (Cofre Chatarra) (Cofre Tesoro)))

mapa2 :: Mapa
mapa2 = Bifurcacion Tesoro
					(Bifurcacion Chatarra (Cofre Chatarra) (Bifurcacion Tesoro (Bifurcacion Chatarra (Cofre Chatarra) (Cofre Tesoro)) (Cofre Chatarra)  ))
					(Bifurcacion Chatarra (Cofre Tesoro) (Cofre Tesoro)) 

mapa3 :: Mapa
mapa3 = Bifurcacion Chatarra
					(Bifurcacion Chatarra (Cofre Chatarra) (Cofre Chatarra))
					(Bifurcacion Tesoro (Cofre Tesoro) (Bifurcacion Chatarra (Cofre Tesoro) (Cofre Tesoro)))
--1.20

hayTesoro :: Mapa -> Bool
hayTesoro (Cofre o) = esTesoro o
hayTesoro (Bifurcacion x m1 m2) = ( esTesoro x) || (hayTesoro m1) || (hayTesoro m2)

esTesoro :: Objeto -> Bool
esTesoro Tesoro = True
esTesoro _ = False

---------------------------------------------------------------------------------------

--1.21

hayTesoroEn :: [Dir] -> Mapa -> Bool
hayTesoroEn [] (Bifurcacion o m1 m2) = esTesoro o
hayTesoroEn [] (Cofre o) = esTesoro o
hayTesoroEn (d:ds) m = if esIzquierda d
						then hayTesoroEn ds (caminoIzquierdo m)
						else hayTesoroEn ds (caminoDerecho m)


esIzquierda :: Dir -> Bool
esIzquierda Izq = True
esIzquierda _ = False

caminoIzquierdo :: Mapa -> Mapa
caminoIzquierdo (Cofre o) = (Cofre o)
caminoIzquierdo (Bifurcacion o m1 m2) = m1

caminoDerecho :: Mapa -> Mapa
caminoDerecho (Cofre o) = (Cofre o)
caminoDerecho (Bifurcacion o m1 m2) = m2

---------------------------------------------------------------------------------------  

--1.22

caminoAlTesoro :: Mapa -> [Dir] 
caminoAlTesoro (Cofre Tesoro) = []
caminoAlTesoro (Bifurcacion o m1 m2) = if hayTesoro m1 
										then [Izq] ++ caminoAlTesoro m1
										else [Der] ++ caminoAlTesoro m2

---------------------------------------------------------------------------------------										

--1.23

caminoRamaMasLarga :: Mapa -> [Dir]
caminoRamaMasLarga (Cofre Tesoro) = []
{-caminoRamaMasLarga (Bifurcacion o m1 (Cofre o)) = [Izq]
caminoRamaMasLarga (Bifurcacion o (Cofre o) m2) = [Der] -}
caminoRamaMasLarga (Bifurcacion o m1 m2) = 	if heightMapa m1 > heightMapa m2
												then [Izq] ++ caminoRamaMasLarga m1
												else [Der] ++ caminoRamaMasLarga m2

heightMapa :: Mapa -> Int
--Denota la altura del arbol
heightMapa (Cofre o) = 0
heightMapa (Bifurcacion x m1 m2) = 1 + (max (heightMapa m1) (heightMapa m2))

---------------------------------------------------------------------------------------

--1.24

tesorosPerLevel :: Mapa -> [[Objeto]]
tesorosPerLevel (Cofre Tesoro) = []
tesorosPerLevel (Bifurcacion o m1 m2) = if esTesoro o
											then [o] : juntarNiveles (tesorosPerLevel m1)(tesorosPerLevel m2)
											else juntarNiveles (tesorosPerLevel m1) (tesorosPerLevel m2)