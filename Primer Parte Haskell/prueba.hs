reemplazar :: Ord a => a -> Int -> [a] -> [a]
reemplazar a n [] = []
reemplazar a n (x:xs) = if (n > 0)
						then x: (reemplazar a (n-1) xs)
						else a :xs


{-
reemplazar :: Ord a => a -> Int -> [a] -> [a]
reemplazar a n [] = []
reemplazar a n (x:xs) = if n == 1
						then a : xs
						else x : (reemplazar a ( n-1) xs) -}