{-
a) O(1)

b) O(1)

c) O(n)

d) O(n)

e) O(n.m)

f) O(n)

g) O(n^2)

h) O(n^2)

i) O(n)

j) O(n)

k) O(n)

l) O(n)

m) O(n)

n) O(n^2)
-}