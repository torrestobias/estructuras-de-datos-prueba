import ConjuntoR
--import Conjunto2

data Tree a = EmptyT | NodeT a (Tree a)(Tree a)

--tree1 :: Tree (Set Int)



set3, set4, set5 :: Set Int
set3 = addS 1( addS 2(addS 3(emptyS)))
set4 = addS 1( addS 5(addS 30(emptyS)))
set5 = addS 30( addS 8(addS 50(emptyS)))


losQuePertenece :: Eq a => [a] -> Set a -> [a]
losQuePertenece [] set = []
losQuePertenece (x:xs) set = if belongs x set
								then x : (losQuePertenece xs set)
								else losQuePertenece xs set

sinRepetidos :: Eq a => [a] -> [a]
sinRepetidos [] = [] 
sinRepetidos xs = setToList ( listToSet xs)

listToSet :: Eq a => [a] -> Set a
listToSet [] = emptyS
listToSet (x:xs) = addS x (listToSet xs)

unirTodos :: Eq a => Tree (Set a) -> Set a
unirTodos EmptyT = emptyS
unirTodos (NodeT x i d) = unionS x (unionS (unirTodos i) (unirTodos d))

