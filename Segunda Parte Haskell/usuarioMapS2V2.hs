import MapS2V2SR

buscarClaves :: Eq k => [k] -> Map k v -> [Maybe v]
-- Busca todas las claves dadas en la lista
buscarClaves [] map = []
buscarClaves (x:xs) map = (lookupM map x) : (buscarClaves xs map)

estanTodas :: Eq k => [k] -> Map k v -> Bool
estanTodas [] map = True
estanTodas (x:xs) map = (elem x (domM map)) && (estanTodas xs map)

actualizarClaves :: Ord k => [(k,v)] -> Map k v -> Map k v
actualizarClaves [] map = map
actualizarClaves (x:xs) map = assocM (actualizarClaves xs map) (fst x) (snd x)

unirDoms :: Eq k => [Map k v] -> [k]
unirDoms [] = []
unirDoms (m:ms) = (domM m) ++ (unirDoms ms)

mapSuccM :: Ord k => [k] -> Map k Int -> Map k Int
--Dada una lista de claves de tipo k y un mapa que va de k a int, le suma uno a cada número
--asociado con dichas claves.
mapSuccM [] map = map
mapSuccM (x:xs) map = assocM (mapSuccM xs map) x (sinJust(lookupM map x) + 1)

sinJust :: Maybe v -> v
sinJust (Just v) = v
sinJust Nothing = error "no se puede sacar"

unirMaps :: Ord k => Map k v -> Map k v -> Map k v
unirMaps map map2 = agregarMap' (domM map) map map2

agregarMap' :: Ord k => [k] -> Map k v -> Map k v -> Map k v
agregarMap' [] map1 map2 = map2
agregarMap' (x:xs) map1 map2 = assocM (agregarMap' xs map1 map2) x (sinJust(lookupM map1 x)) 


indexar :: Eq a => [a] -> Map Int a
indexar xs = crearMap (agregarIndices 0 xs) -- 0 es el primer indice

crearMap :: Ord k => [(k,v)] -> Map k v
crearMap [] = emptyM
crearMap (k:kvs) = assocM (crearMap kvs) (fst k) (snd k)

agregarIndices :: Eq a => Int -> [a] -> [(Int,a)]
agregarIndices n [] = []
agregarIndices n (x:xs) = (n,x) : (agregarIndices (n+1) xs)




ocurrencias :: String -> Map Char Int
--Dado un string cuenta las ocurrencias de cada caracter utilizando un MAP
ocurrencias xs = crearMap (armarTupla xs) 

armarTupla :: String -> [(Char,Int)]
armarTupla [] = []
armarTupla (x:xs) = (x,(contarApariciones x xs)+1) : (armarTupla xs)

contarApariciones :: Eq a => a -> [a] -> Int
contarApariciones a [] = 0
contarApariciones a (x:xs) = if (a == x)
								then 1 + (contarApariciones a xs)
								else contarApariciones a xs
