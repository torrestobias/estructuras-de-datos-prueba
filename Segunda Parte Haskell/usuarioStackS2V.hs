import StackS2V1

data Tree a = EmptyT | NodeT a (Tree a)(Tree a)

stack1, stack2, stack3 :: Stack Int
stack1 = push 4(push 3(push 2(push 1(emptyS))))
stack2 = push 9(push 8(push 7(push 6(emptyS))))
stack3 = push 13(push 12(push 11(push 10(emptyS))))

{-
tree :: Tree (Stack Int)
tree = NodeT stack1 ( EmptyT)
					(stack2 EmptyT EmptyT)

stack1 :: Stack Int
stack1 = push 1(push 2(push 3(emptyS)))
-}

apilar :: [a] -> Stack a
apilar xs = apilar' (reverse xs)

apilar' :: [a] -> Stack a
apilar' [] = emptyS
apilar' (x:xs) = push x (apilar' xs)


desapilar :: Stack a -> [a]
desapilar stack = reverse (desapilar' stack)

desapilar' :: Stack a -> [a]
desapilar' stack = if (isEmptyS stack)
					then []
					else (top stack) : (desapilar' (pop stack))

treeToStack :: Tree a -> Stack a
treeToStack EmptyT = emptyS
treeToStack arbol =  apilar(listInOrder arbol)


listInOrder :: Tree a -> [a]
listInOrder EmptyT = []
listInOrder (NodeT x i d) = (listInOrder i) ++ [x] ++ (listInOrder d)
