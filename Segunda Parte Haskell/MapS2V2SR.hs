module MapS2V2SR (Map, emptyM, assocM, lookupM, deleteM, domM) where

--Sin Repetidos

data Map k v = M [(k,v)] deriving (Show,Eq,Ord)

map :: Map Int Int
map = M [(1,10),(2,30)]

--O(1)
emptyM :: Map k v
emptyM = M []

--O(N)
assocM :: Ord k => Map k v -> k -> v -> Map k v
assocM (M ksv) k v = M (agregar k v ksv)

--O(N)
agregar :: Ord k => k -> v -> [(k,v)] -> [(k,v)]
agregar k v [] = [(k,v)]
agregar k v (k':ksv) = if (k == (fst k'))
						then (fst k', v) : ksv
						else (fst k', snd k') : (agregar k v ksv)

--O(N)
lookupM :: Eq k => Map k v -> k -> Maybe v
lookupM (M kvs) k = (lookupM' kvs k)

--O(N)
lookupM' :: Eq k => [(k,v)] -> k -> Maybe v
lookupM' [] k = Nothing
lookupM' (x:xs) k = if ((fst x) == k)
					 then Just (snd x)
					 else lookupM' xs k

--O(N)
deleteM :: Eq k => Map k v -> k -> Map k v
deleteM (M ks) k = M (deleteM' ks k)

--O(N)
deleteM' :: Eq k => [(k,v)] -> k -> [(k,v)]
deleteM' [] k = []
deleteM' (x:xs) k = if ((fst x) == k)
					then deleteM' xs k
					else x : (deleteM' xs k)

--O(N)
domM :: Map k v -> [k]
domM (M kvs) = devolverClaves kvs

--O(N)
devolverClaves :: [(k,v)] -> [k]
devolverClaves [] = []
devolverClaves (x:xs) = (fst x) : (devolverClaves xs)