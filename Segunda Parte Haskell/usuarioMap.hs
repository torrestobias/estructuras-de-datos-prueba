import Map

buscarClaves :: Eq k => [k] -> Map k v -> [Maybe v]
buscarClaves [] map = []
buscarClaves (k:ks) map = [(lookupM map k)] ++ (buscarClaves ks map)

------------------------------------------------------------------------------

estanTodas :: Eq k => [k] -> Map k v -> Bool
-- indica si en el map se encuentran todas las claves dadas
estanTodas [] map = False
estanTodas (k:ks) map = (elem k (domM map)) || (estanTodas ks map)

------------------------------------------------------------------------------

actualizarClaves :: Eq k => [(k,v)] -> Map k v -> Map k v
actualizarClaves [] map = map
actualizarClaves (k:ks) map = assocM (actualizarClaves ks map) (fst k) (snd k)

------------------------------------------------------------------------------

unirDoms :: Eq k => [ Map k v] -> [k]
unirDoms maps = sinRepetidos(juntarDoms maps)

juntarDoms :: Eq k => [Map k v] -> [k]
juntarDoms [] = []
juntarDoms (m : maps) = domM m ++ (juntarDoms maps)

sinRepetidos :: Eq a => [a] -> [a]
sinRepetidos [] = []
sinRepetidos (x : xs) = if elem x xs then sinRepetidos xs else x : (sinRepetidos xs)

------------------------------------------------------------------------------

mapSuccM :: Ord k => [k] -> Map k Int -> Map k Int
--Dada una lista de claves de tipo "k" y un mapa que va de "k" a int, le suma uno a cada numero asociado con dichas claves.
mapSuccM ks map = sumarATodas ks map

sumarATodas :: Ord k  => [k] -> Map k Int -> Map k Int
sumarATodas [] m = m
sumarATodas (k:ks) m = sumarATodas ks (assocM m k ( (obtenerValor k m)+1 ) )

obtenerValor :: Ord k => k -> Map k Int -> Int
obtenerValor k m = sinJust (lookupM m k) 

sinJust :: Maybe a -> a
sinJust Nothing = error "no tiene valor"
sinJust (Just v) = v 


{-

mapSuccM :: Ord k  => [k] -> Map k Int -> Map k Int
mapSuccM ks m = sumarATodas ks m

sumarATodas :: Ord k  => [k] -> Map k Int -> Map k Int
sumarATodas [] m = m
sumarATodas (k:ks) m = sumarATodas ks (assocM m k ( (obtenerValor k m)+1 ) )

obtenerValor :: Ord k => k -> Map k Int -> Int
obtenerValor k m = sinJust (lookupM m k) 

sinJust :: Maybe a -> a
sinJust Nothing = error "no tiene valor"
sinJust (Just v) = v -}