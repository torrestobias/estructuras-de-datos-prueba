module Cola (Queue , emptyQ , isEmptyQ , queue , firstQ , dequeue) where

import Stack

data Queue a = Q fs bs  -- Si fs esta vacia, bs esta vacia.

--crea una cola vacia.
emptyQ :: Queue a
emptyQ = Q emptyS emptyS

--dada una cola indica si la cola esta vacia.
isEmptyQ :: Queue a -> Bool
isEmptyQ (Q fs bs) = null fs

--dados un elemento y una cola, agrega ese elemento a la cola.
queue :: a -> Queue a -> Queue a
queue e (Q fs bs) = Q fs (push e bs)

--dada una cola devuelve el primer elemento de la cola.
firstQ :: Queue a -> a
firstQ (Q fs bs) = head fs

--dada una cola la devuelve sin su primer elemento.
dequeue :: Queue a -> Queue a
dequeue (Q fs bs) = controlar fs bs


controlar :: [a] -> [a] -> Queue a
controlar [] bs = emptyQ
controlar fs bs = Q (tail fs) bs

--devuelve la cantidad de elementos.
lenQ :: Queue a -> Int
lenQ (S fs bs) = length fs + length bs