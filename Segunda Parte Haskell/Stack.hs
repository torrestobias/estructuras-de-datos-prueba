module Stack (Stack, emptyS, isEmptyS, push, top, pop, stack1, stack2,lenS) where


data Stack a = S [a] Int deriving (Show,Eq)

stack1 :: Stack Int
stack1 = S [20] 1

stack2 :: Stack Int
stack2 = S [1,2,3,444] 4

emptyS :: Stack a
--crea una pila vacia
emptyS = S [] 0

isEmptyS :: Stack a -> Bool
-- dada una pila indica si esta vacia
isEmptyS (S pila n) = null pila

push :: a -> Stack a -> Stack a
-- dados un elemento y una pila , agrega el elemento a la pila
push e (S pila n)  = S ((pila) ++ [e]) (n+1)

top :: Stack a -> a
-- dada una pila devuelve el elemento del tope de la fila 
top (S pila n) = head pila 

pop :: Stack a -> Stack a
-- dada pila devuelve la pila sin el primer elemento
pop (S pila n) = S (tail pila) (n-1)

lenS :: Stack a -> Int
lenS (S pila n) = n 

{-maxS :: Ord a => Stack a -> a
-- devuelve el primer elemento maximo de la lista
maxS (S xs) = maximoS xs

maximoS :: [a] -> a
maximoS xs = if null xs
				then error "x"
				else last xs -}