module Queue2Listas(Queue,emptyQ, isEmptyQ, queue, firstQ, dequeue, lenQ ) where

data Queue a = Q [a] [a] deriving(Show,Eq)

--Inv.rep : 
-- si la primer lista se encuentra vacia , la cola se encuentra vacia
-- fs front stack y bs back stack

emptyQ :: Queue a
-- crea una cola vacia
emptyQ = Q [] []

isEmptyQ :: Queue a -> Bool
-- dada una cola indica si esta vacia. null es una funcion primitiva de haskell, devuelve un booleano si una lista esta vacia
isEmptyQ (Q fs bs) = null fs

queue :: a -> Queue a -> Queue a
-- Dados un elemento y una cola, agrega ese 	elemento a la cola.
queue a (Q fs bs) = if (null fs)
					then Q (fs ++ [a]) bs
					else Q (fs) (bs ++ [a])

firstQ :: Queue a -> a 
-- Dada una cola devuelve el primer elemento de la cola.
-- PREGUNTAR!!!
firstQ (Q fs bs) = head fs


--O(1) amortizado, esto es cuando cada tanto es lineal.
dequeue :: Queue a -> Queue a
--Dada una cola la devuelve sin su primer elemento.
dequeue (Q fs bs) = if (null fs)
					then error "Cola Vacia"
					else if (length fs == 1)
							then Q (reverse bs) []
							else Q (tail fs) bs
lenQ :: Queue a -> Int
lenQ (Q fs bs) = length fs + length bs