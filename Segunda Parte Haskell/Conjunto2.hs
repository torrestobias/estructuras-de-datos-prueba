module Conjunto2 (Set, emptyS, addS, belongs, sizeS,removeS, unionS, intersectionS, setToList,set1) where

data Set a = S [a] Int deriving(Show,Eq) -- Inv. los elementos de la lista no estan repetidos

set1 :: Set Int
set1 = S [1,2,3] 3

emptyS :: Set a
emptyS = S [] 0

-- O(n)
addS :: Eq a => a -> Set a -> Set a
addS a (S xs n) = if elem a xs
					then S xs n
					else S (a : xs) (n+1)

--O(n)
belongs :: Eq a => a -> Set a -> Bool
belongs a (S xs n) = elem a xs

--O(1)
sizeS :: Eq a => Set a -> Int
sizeS (S xs n) = n

--O(n)
removeS :: Eq a => a -> Set a -> Set a
removeS a (S xs n) = if elem a xs
						then S (removeL a xs) (n-1)
						else S xs n 

removeL :: Eq a => a -> [a] -> [a]
removeL a (y:ys) = if a == y
					then ys
					else y : (removeL a ys)

--unionS (S xs n)(S ys m) = S (xs ++ ys) (n + m)

--O(n^2)
unionS :: Eq a => Set a -> Set a -> Set a
unionS (S xs n) st = addAll xs st

addAll :: Eq a => [a] -> Set a -> Set a
addAll [] st = st
addAll (x:xs) st = addS x (addAll xs st)


intersectionS :: Eq a => Set a -> Set a -> Set a
intersectionS (S xs _)(S ys _) = armarSet (interseccionL xs ys)

interseccionL :: Eq a => [a] -> [a] -> [a]
interseccionL [] ys = []
interseccionL (x:xs) ys = if elem x ys
							then x : (interseccionL xs ys)
							else (interseccionL xs ys)

armarSet :: Eq a => [a] -> Set a
armarSet [] = emptyS
armarSet (x:xs) = addS x (armarSet xs)

setToList :: Eq a => Set a -> [a]
setToList (S xs n) =  xs


