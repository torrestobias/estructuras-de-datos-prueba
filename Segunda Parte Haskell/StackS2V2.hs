module StackS2V2 (Stack, emptyS, isEmptyS, push, top, pop) where

data Stack a = S [a] [a] deriving (Show,Eq,Ord) --Inv. en la segunda lista esta el elemento mayor agregado


stackPrueba :: Stack Int
stackPrueba = S [1,2,3] [1,2,3]

--O(1)
emptyS :: Stack a
emptyS = S [] []

--O(1)
isEmptyS :: Stack a -> Bool
isEmptyS (S xs ms) = null xs

--O(n)
push :: Ord a => a -> Stack a -> Stack a
push a (S pila1 pila2) = if (null pila2)
							then S (pila1 ++ [a])(pila2 ++ [a])
							else (S (pila1 ++ [a])(pila2 ++ [(max a (last pila2))] ))


--O(1)
top :: Stack a -> a
top (S xs ms) = last xs

--O(1)
pop :: Stack a -> Stack a
pop (S xs ms) = S (init xs) (init ms)

--O(n)
maxS :: Ord a => Stack a -> a
maxS (S xs ms) = head ms
