module Queue ( Queue,emptyQ, isEmptyQ) where

import Stack

data Queue a = Q (Stack a) (Stack a) Int deriving (Show,Eq) -- Inv.rep = si fs se encuentra vacia, entonces la cola esta vacia.

queue2 :: Queue Int
queue2 = Q (stack1)(stack2) 5
----------------------------------------------------------------------------------------------------
emptyQ :: Queue a
-- crea una cola vacia
emptyQ = Q (emptyS) (emptyS) 0


isEmptyQ :: Eq a => Queue a -> Bool 
-- dada una cola indica si esta vacia. null es una funcion primitiva de haskell, devuelve un booleano si una lista esta vacia
isEmptyQ q = q == emptyQ

queue :: a -> Queue a -> Queue a
 -- dado un elemento a y una cola, agrega el elemento a la cola
queue e (Q fs bs n) = if isEmptyS fs
						then (Q (push e fs) bs) (n+1)
						else (Q fs ( push e bs)) (n + 1)


dequeue :: Queue a -> Queue a
--Dada una cola la devuelve sin su primer elemento.
dequeue (Q fs bs n) =  if (isEmptyS fs)
						then error "no se puede"
						else if lenS fs == 1
						then (Q (apilar(reverse(desapilar bs))) emptyS) (n-1)
						else (Q (pop fs) bs) (n - 1)

firstQ :: Queue a -> a
firstQ (Q fs bs n) = if isEmptyS fs
						then error "no se puede"
						else top fs


lenQ :: Queue a -> Int 
-- Devuelve la cantidad de elementos
lenQ (Q fs bs n) = n 

apilar :: [a] -> Stack a
--Dada una lista devuelve una pila sin alterar el orden de los elementos.
apilar [] = emptyS
apilar xs = push (last xs) (apilar (init xs))

desapilar :: Stack a -> [a]
-- Dada una pila devuelve una lista sin alterar el orden de los elementos.
desapilar pila = if isEmptyS pila
					then []
					else top pila : (desapilar (pop pila))

