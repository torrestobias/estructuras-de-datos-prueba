module SoloQ3(Queue,emptyQ,isEmptyQ, queue, firstQ, dequeue, lenQ) where

data Queue a = Q [a] Int

--O(1)
emptyQ :: Queue a
emptyQ = Q [] 0

--O(1)
isEmptyQ :: Queue a -> Bool
isEmptyQ (Q xs n) = estaVacia xs

estaVacia :: [a] -> Bool
estaVacia [] = True
estaVacia _ = False

--O(1)
queue :: a -> Queue a -> Queue a
queue a (Q xs n) = Q (a:xs) (n+1)

--O(1)
firstQ :: Queue a -> a
firstQ (Q xs n) = head xs

--O(1)
dequeue :: Queue a -> Queue a
dequeue (Q xs n) = Q (init xs) (n-1)

--O(1)
lenQ :: Queue a -> Int
lenQ (Q xs n) = n