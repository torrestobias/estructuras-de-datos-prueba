import MultiSet

ocurrencias :: String -> MultiSet Char
ocurrencias xs = (armarMultiSet xs)

armarMultiSet :: String -> MultiSet Char
armarMultiSet [] = emptyMS
armarMultiSet (x:xs) = addMS x (armarMultiSet xs)