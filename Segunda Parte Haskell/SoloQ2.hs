module SoloQ2(Queue,emptyQ, isEmptyQ, queue, firstQ, dequeue) where

data Queue a = Q [a] --Inv.Rep

--O(1)
emptyQ :: Queue a
emptyQ = Q []

--O(1)
isEmptyQ :: Queue a -> Bool
isEmptyQ (Q xs) = estaVacia xs

estaVacia :: [a] -> Bool
estaVacia [] = True
estaVacia _ = False

--O(1) PREGUNTAR!!!
queue :: a -> Queue a -> Queue a
queue a (Q xs) = Q (a:xs)

--O(1)
firstQ :: Queue a -> a
firstQ (Q xs) = head xs

--O(1)
dequeue :: Queue a -> Queue a
dequeue (Q xs) = Q (init xs)