module MapS2V2 (Map,emptyM, assocM, lookupM, deleteM, domM) where

--Con repetidos

data Map k v = M [(k,v)] deriving (Show,Eq)


map1 = M [(1,2),(10,3),(11,4)]

emptyM :: Map k v
emptyM = M []

--O(1)
assocM :: Eq k => Map k v -> k -> v -> Map k v
assocM (M ks) k v = M ( (k,v): ks)

lookupM :: Eq k => Map k v -> k -> Maybe v
lookupM (M ks) k = (lookupM' ks k)

lookupM' :: Eq k => [(k,v)] -> k -> Maybe v
lookupM' [] k = Nothing
lookupM' (x:xs) k = if ( (fst x) == k)
					then Just (snd x)
					else lookupM' xs k 

deleteM :: Eq k => Map k v -> k -> Map k v
deleteM (M ks) k = M (deleteM' ks k)

deleteM' :: Eq k => [(k,v)] -> k -> [(k,v)]
deleteM' [] k = []
deleteM' (x:xs) k = if ((fst x) == k)
					then deleteM' xs k
					else x : (deleteM' xs k)

domM :: Map k v -> [k]
domM (M ks) = (devolverClaves ks)

devolverClaves :: [(k,v)] -> [k]
devolverClaves [] = []
devolverClaves (x:xs) = (fst x) : (devolverClaves xs)

