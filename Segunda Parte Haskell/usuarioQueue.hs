import Queue
{-
largoQ :: Queue a -> Int
-- cuenta la cantidad de elementos que hay en la cola (iiiiiiiiiiiiiii).
largoQ cola = if isEmptyQ cola then 0 else 1 + largoQ (dequeue cola) -}

-- O (1)
largoQ :: Queue a -> Int
largoQ cola = lenQ cola


atender :: Queue String -> [String]
atender cola = if isEmptyQ cola
				 then [] 
				 else firstQ cola : atender ( dequeue cola)

unirQ :: Queue a -> Queue a -> Queue a
unirQ cola1 cola2 = if isEmptyQ cola2
						then cola1
						else unirQ (queue (firstQ cola2) cola1) (dequeue cola2)