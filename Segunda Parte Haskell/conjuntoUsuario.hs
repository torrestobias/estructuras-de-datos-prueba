import Conjunto

data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving (Show)

arbol1 :: Tree (Set Int)

arbol1 = NodeT (set3) (NodeT (set4) EmptyT (NodeT (set4) EmptyT EmptyT)) (NodeT (set5) EmptyT EmptyT)

set3, set4, set5 :: Set Int

set3 = addS 3(addS 2 (addS 1 (emptyS)))
set4 = addS 4 (addS 5 (addS 6 (emptyS)))
set5 = addS 1 (addS 3 (addS 5 (emptyS)))


losQuePertenecen :: Eq a => [a] -> Set a -> [a]
-- Dados una lista y un conjunto, devuelve una lista con todos los elementos que pertenecen al conjunto.
losQuePertenecen [] set = [] 
losQuePertenecen (x:xs) set = if belongs x set
								then x  : (losQuePertenecen xs set)
								else losQuePertenecen xs set
								
sinRepetidos :: Eq a => [a] -> [a]
sinRepetidos [] = []
sinRepetidos xs =  setToList (listToSet xs)


listToSet :: Eq a => [a] -> Set a
listToSet [] = emptyS
listToSet (x:xs) = addS x ( listToSet xs)


unirTodos :: Eq a => Tree (Set a) -> Set a
-- Dado un arbol de conjuntos devuelve un conjunto con la union de todos los conjuntos del arbol.
unirTodos EmptyT = emptyS
unirTodos (NodeT set t1 t2) = listToSet(sinRepetidos(setToList(unionS set (unionS (unirTodos t1)(unirTodos t2)))))
