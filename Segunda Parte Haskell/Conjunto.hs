module Conjunto (Set, emptyS, addS, belongs, sizeS, removeS, unionS, intersectionS, setToList,set1, maximoC) where

import Data.List

data Set a = Set [a]  -- INV. de Representación: los elementos de la lista no están repetidos

set1 :: Set Int
set1 = Set [1,2,3,4]




-- import Set as st es una manera de nombrar la interfaz

--set2 :: Set 

emptyS :: Set a --Crea un conjunto vacio 

addS :: Eq a => a -> Set a -> Set a -- Dados un elemento y un conjunto, agrega el elemento al conjunto.

belongs :: Eq a => a -> Set a -> Bool --Dados un elemento y un conjunto indica si el elemento pertenece al conjunto.

sizeS :: Eq a => Set a -> Int -- DDevuelve la cantidad de elementos distintos de un conjunto.

removeS :: Eq a => a -> Set a -> Set a -- Dado un elemento y un conjunto, elimina el elemento del conjunto

unionS :: Eq a => Set a -> Set a -> Set a --Dado dos conjuntos, devuelve uno nuevo con elementos de ambos

intersectionS :: Eq a => Set a -> Set a -> Set a -- Dado dos conjuntos, devuelve uno nuevo con elementos en comun de ambos

setToList :: Eq a => Set a -> [a] --Dado un conjunto devuelve una lista con todos los elementos del conjunto.

maximoC :: Ord a => Set a -> a -- devuelve el maximo elemento de un conjunto

---------------------------------------------------------------------------------------------

emptyS = Set []

-- O(n)
addS x (Set xs) = if not (elem x xs) then Set (x:xs) else Set (xs)

-- O(n)
belongs x (Set xs) = elem x xs

-- O(n)
sizeS (Set xs) = length xs

-- O(n)
removeS x (Set xs) = Set (delete x xs)

-- O(n)
unionS (Set xs) (Set ys) = Set (xs ++ ys)

-- O(n*m)
intersectionS (Set xs) (Set ys) = Set (intersection xs ys)

-- O(n*m)
intersection :: Eq a => [a] -> [a] -> [a]
intersection [] xs = []
intersection (x:xs) ys = if elem x ys then x:intersection xs ys else intersection xs ys

-- O(1)
setToList (Set xs) = xs

-- O (1)
maximoC (Set xs) = last xs