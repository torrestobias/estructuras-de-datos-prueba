module MapS2V2CL (Map, emptyM, assocM) where

--Con dos listas 

-- donde en la posicion i de la lista de claves 
-- esta la clave del valor i de la lista de valores

data Map k v = M [k] [v] deriving (Show,Eq) 

mapPrueba :: Map Int Int
mapPrueba = M [1,2][3,4]

emptyM :: Map k v
emptyM = M [] []

assocM :: Eq k => Map k v -> k -> v -> Map k v
assocM (M ks vs) k v = armarDesdeTupla (agregarKV k v ks vs)

armarDesdeTupla :: ([k],[v]) -> Map k v
armarDesdeTupla (ks,vs) = M ks vs



agregarKV :: Eq k => k -> v -> [k] -> [v] -> ([k],[v])
agregarKV nk nv [] [] = ([nk],[nv])
agregarKV nk nv (k:ks)(v:vs) = if ( nk == k)
								then ((k:ks),(nv:vs))
								else (agregarKV nk nv (k:ks) (v:vs))
