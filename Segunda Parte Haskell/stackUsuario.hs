import Stack

data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving (Show)

arbol1 :: Tree Int

arbol1 = NodeT 30 (NodeT 20 EmptyT (NodeT 2 EmptyT EmptyT)) (NodeT  90 EmptyT EmptyT)

apilar :: [a] -> Stack a
--Dada una lista devuelve una pila sin alterar el orden de los elementos.
apilar [] = emptyS
apilar xs = push (last xs) (apilar (init xs))

desapilar :: Stack a -> [a]
-- Dada una pila devuelve una lista sin alterar el orden de los elementos.
desapilar pila = if isEmptyS pila
					then []
					else top pila : (desapilar (pop pila))

treeToStack :: Tree a -> Stack a
-- Dado un árbol devuelve una pila con los elementos apilados inorder.
treeToStack EmptyT = emptyS
treeToStack t = apilar (listInOrder t)

listInOrder :: Tree a -> [a]
listInOrder EmptyT = []
listInOrder (NodeT x ti td) = (listInOrder ti) ++ [x] ++ (listInOrder td) 

