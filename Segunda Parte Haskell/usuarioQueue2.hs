--import SoloQ
--import SoloQ3
import Queue2Listas

soloDuo :: Queue Int
soloDuo = queue 1 (queue 2(queue 3(emptyQ)))

soloDuo2 :: Queue Int
soloDuo2 = queue 20 (queue 30(queue 40(emptyQ)))

lengthQ :: Queue a -> Int
lengthQ queue = length (queueToList queue)

lengthQ' :: Queue a -> Int
lengthQ' queue = (lenQ queue)

queueToList :: Queue a -> [a]
queueToList queue = if isEmptyQ queue
							then []
							else (firstQ queue) : (queueToList (dequeue queue))

unionQ :: Queue a -> Queue a -> Queue a
unionQ queue1 queue2 = if isEmptyQ queue2
						then queue1
			else (unionQ (queue(firstQ queue2) queue1) (dequeue queue2))
						