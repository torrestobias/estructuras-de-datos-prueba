module Stack2 (Stack, emptyS, isEmptyS, push, top, pop, stack1) where


data Stack a = S [a] [a] deriving (Show,Eq)

stack1 :: Stack Int
stack1 = S [1,2,3,4] [1]

stack2 :: Stack Int
stack2 = S [1,2,3] [3,2,2]

emptyS :: Stack a
--crea una pila vacia
emptyS = S [] [] 

isEmptyS :: Stack a -> Bool
-- dada una pila indica si esta vacia
isEmptyS (S pila pila2) = null pila 

push :: Ord a => a -> Stack a -> Stack a
-- dados un elemento y una pila , agrega el elemento a la pila
push e (S pila pila2) = if null pila2
						then S (e : pila)(e : pila2)
						else S (e : pila ) ( (max e (head pila2) ) : pila2 )

top :: Stack a -> a
-- dada una pila devuelve el elemento del tope de la fila 
top (S pila pila2) = (head pila) 

pop :: Stack a -> Stack a
-- dada pila devuelve la pila sin el primer elemento
pop (S pila pila2) = S (tail pila) pila2

