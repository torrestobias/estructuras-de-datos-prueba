module Map(Map, emptyM, lookupM, assocM, deleteM, domM,map1,map2,map3) where

import Data.Maybe

data Map k v = M [(k, v)] deriving (Show,Eq)

map1 :: Map String Int
map1 = M [("hola",1),("hola",3),("adios",88),("mu",7)]

map2 :: Map String Int
map2 = M [("jaja",1),("haskell",3),("arg",88),("led",7)]

map3 :: Map Int Int
map3 = M [(1,23)]

emptyM :: Map k v
emptyM = M []

lookupM :: Eq k => Map k v -> k -> Maybe v
--dado un map y una clave, devuelve el valor
lookupM (M xs) k = lookup k xs

assocM :: Eq k => Map k v -> k -> v ->  Map k v
-- dado un map, una clave y un valor, devuelve la clave y el valor agregados al map
assocM (M xs) k v = M ((k,v) : xs) 

deleteM :: Eq k => Map k v -> k -> Map k v
-- dado un map y una clave, borra la clave del map y devuelve un map
deleteM (M xs) k = M (borrar k xs)

borrar k [] = []
borrar k (x:xs) =
   if k == fst x
      then borrar k xs
      else x : borrar k xs
	  
domM :: Eq k => Map k v -> [k]
-- dado un map devuelve todas las claves de este
domM (M xs) = obtenerClaves xs

obtenerClaves [] = []
obtenerClaves (x:xs) = fst x : obtenerClaves xs

