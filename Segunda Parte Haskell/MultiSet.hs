module MultiSet (MultiSet, 
	emptyMS,
	addMS,
	ocurrencesMS,
	unionMS,
	intersectionMS,
	multiSetToList,
	)
where

--Inv.Rep 

import MapS2V2SR

multiSet1 :: MultiSet Char
multiSet1 = Ms (assocM (assocM(assocM emptyM 'a' 3) 'b' 2) 'm' 1)

multiSet2 :: MultiSet Char
multiSet2 = Ms (assocM (assocM(assocM emptyM 'a' 1) 'x' 1) 'z' 1)
{-
Funciones de map:
*assocM :: Ord k => Map k v -> k -> v -> Map k v 
*lookupM :: Eq k => Map k v -> k -> Maybe v
*deleteM :: Eq k => Map k v -> k -> Map k v
*domM :: Map k v -> [k]
-}
data MultiSet a = Ms (Map a Int) deriving(Show)

emptyMS :: MultiSet a
emptyMS = Ms (emptyM)

---------------------------------------------------------------------

addMS :: Ord a => a -> MultiSet a -> MultiSet a
addMS a (Ms map) = Ms (assocM map a (sinJust(lookupM map a )+1))
					
sinJust :: Maybe Int -> Int
sinJust (Just n) = n
sinJust Nothing = 0


---------------------------------------------------------------------

ocurrencesMS :: Ord a => a -> MultiSet a -> Int
ocurrencesMS a (Ms map) = (sinJust(lookupM map a)+1)


---------------------------------------------------------------------

unionMS :: Ord a => MultiSet a -> MultiSet a -> MultiSet a 
unionMS (Ms map1) (Ms map2) = Ms (unirMaps map1 map2)

unirMaps :: Ord k => Map k Int -> Map k Int -> Map k Int
unirMaps map map2 = agregarMap (domM map) map map2

agregarMap :: Ord k => [k] -> Map k Int -> Map k Int -> Map k Int
agregarMap [] map1 map2 = map2
agregarMap (x:xs) map1 map2 = assocM (agregarMap xs map1 map2) x ((sacarJust(lookupM map1 x)+1))

sacarJust :: Maybe v -> v
sacarJust (Just v) = v
sacarJust Nothing = error "no se puede sacar"

---------------------------------------------------------------------

intersectionMS :: Ord a => MultiSet a -> MultiSet a -> MultiSet a 
intersectionMS (Ms map1)(Ms map2) = Ms (intersectarMaps map1 map2)

intersectarMaps :: Ord k => Map k Int -> Map k Int -> Map k Int
intersectarMaps map1 map2 = agregarMapSiSonIguales (interseccion(domM map1)(domM map2)) map1 map2

agregarMapSiSonIguales :: Ord k => [k] -> Map k Int -> Map k Int -> Map k Int
agregarMapSiSonIguales [] map1 map2 = emptyM
agregarMapSiSonIguales (x:xs) map1 map2 = if (elem x (domM map2))
											then assocM (agregarMapSiSonIguales xs map1 map2) x ((sacarJust(lookupM map1 x)+1))
											else agregarMapSiSonIguales xs map1 map2

interseccion :: Eq a => [a] -> [a] -> [a]
interseccion [] [] = []
interseccion [] ys = []
interseccion xs [] = []
interseccion (x:xs)(y:ys) = if (x == y)
								then x : interseccion xs ys
								else interseccion xs ys

---------------------------------------------------------------------

multiSetToList :: Eq a => MultiSet a -> [(Int,a)]
multiSetToList (Ms map) = convertirALista (domM map) map

convertirALista :: Eq k => [k] -> Map k Int -> [(Int,k)]
convertirALista [] map = []
convertirALista (x:xs) map = (sinJust(lookupM map x),x) : (convertirALista xs map)
