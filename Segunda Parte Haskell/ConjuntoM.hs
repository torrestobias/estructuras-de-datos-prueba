module ConjuntoM (Set, emptyS, addS, belongs, sizeS,removeS, unionS, intersectionS, setToList,set1) where

data Set a = S [a] deriving(Show,Eq) 

set1 :: Set Int
set1 = S [1,2,3]

--O(1)
emptyS :: Set a
emptyS = S []

-- O(n)
addS :: Eq a => a -> Set a -> Set a
addS a (S xs ) = S (a : xs)

--O(n)
belongs :: Eq a => a -> Set a -> Bool
belongs a (S xs) = elem a xs

--O(n)
sizeS :: Eq a => Set a -> Int
sizeS (S xs) = length xs

--O(n)
removeS :: Eq a => a -> Set a -> Set a
removeS a (S xs) = if elem a xs
						then S (removeL a xs)
						else S xs

removeL :: Eq a => a -> [a] -> [a]
removeL a (y:ys) = if a == y
					then ys
					else y : (removeL a ys)

--unionS (S xs n)(S ys m) = S (xs ++ ys) (n + m)

--O(n^2)
unionS :: Eq a => Set a -> Set a -> Set a
unionS (S xs) st = addAll xs st

addAll :: Eq a => [a] -> Set a -> Set a
addAll [] st = st
addAll (x:xs) st = addS x (addAll xs st)


intersectionS :: Eq a => Set a -> Set a -> Set a
intersectionS (S xs)(S ys) = armarSet (interseccionL xs ys)

interseccionL :: Eq a => [a] -> [a] -> [a]
interseccionL [] ys = []
interseccionL (x:xs) ys = if elem x ys
							then x : (interseccionL xs ys)
							else (interseccionL xs ys)

armarSet :: Eq a => [a] -> Set a
armarSet [] = emptyS
armarSet (x:xs) = addS x (armarSet xs)

setToList :: Eq a => Set a -> [a]
setToList (S xs) =  xs

maximoC :: Ord a => Set a -> a
maximoC (S xs) = esteEsElMaximo xs

esteEsElMaximo :: Ord a => [a] -> a
esteEsElMaximo [a] = a
esteEsElMaximo (x:xs) = max x (esteEsElMaximo xs)