module StackS2V1(Stack, emptyS, isEmptyS, push, top, pop) where

data Stack a = S [a] deriving (Show,Eq) 

emptyS :: Stack a
emptyS = S []

isEmptyS :: Stack a -> Bool
isEmptyS (S xs) = null xs

push :: a -> Stack a -> Stack a
push a (S xs) = S (agregarAlFinalDelStack a xs)

agregarAlFinalDelStack :: a -> [a] -> [a]
agregarAlFinalDelStack a [] = [a]
agregarAlFinalDelStack a xs = xs ++ [a]

top :: Stack a -> a
top (S xs) = last xs

pop :: Stack a -> Stack a
pop (S xs) = S (init xs)

