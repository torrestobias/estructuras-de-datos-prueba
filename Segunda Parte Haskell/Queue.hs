module Queue ( Queue,emptyQ, isEmptyQ, queue, firstQ, dequeue, lenQ, queue1, queue2, queue3) where

data Queue a = Q [a] Int deriving (Show,Eq) -- Invariante de representacion N debe ser el largo de la lista

queue1 :: Queue Int
queue1 = Q [1,2,3,4] 4

queue3 :: Queue Int
queue3 = Q [5,6,7] 3

queue2 :: Queue String
queue2 = Q ["Jarvan IV","Yorick","Brand","Karma"] 4


----------------------------------------------------------------------------------------------------

emptyQ :: Queue a
-- crea una cola vacia
emptyQ = Q [] 0 

isEmptyQ :: Queue a -> Bool 
-- dada una cola indica si esta vacia. null es una funcion primitiva de haskell, devuelve un booleano si una lista esta vacia
isEmptyQ (Q xs n) = null xs

queue :: a -> Queue a -> Queue a
-- Dados un elemento y una cola, agrega ese elemento a la cola.
queue a (Q xs n) = Q (agregarElementoAlFinal xs a) (n + 1)

agregarElementoAlFinal :: [a] -> a -> [a]
-- dada una lista y un elemento, agrega el elemento al final de la lista
agregarElementoAlFinal xs a = xs ++ [a]

firstQ :: Queue a -> a 
-- Dada una cola devuelve el primer elemento de la cola.
firstQ (Q xs n) = head xs

dequeue :: Queue a -> Queue a
--Dada una cola la devuelve sin su primer elemento.
dequeue (Q xs n) =  Q (tail xs) (n -1)

lenQ :: Queue a -> Int 
-- Devuelve la cantidad de elementos
lenQ (Q xs n) = n

